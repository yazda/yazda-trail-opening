App.info({
  id:          'com.yazda.trail_status_android',
  name:        'Trail Status',
  description: 'See what mountain bike trails are open in your area',
  version:     '2.0.7',
  website:     'https://trail-status.com'
});

App.setPreference('Orientation', 'portrait');
App.setPreference('deployment-target', '8.0');
App.setPreference('android-minSdkVersion', '15');
App.setPreference('StatusBarOverlaysWebView', 'true');
App.setPreference('StatusBarStyle', 'lightcontent');
App.setPreference('target-device', 'handset');

App.accessRule('http://*', {launchExternal: true});
App.accessRule('https://*', {launchExternal: true});
App.accessRule('*://trail-status.com/*');
App.accessRule('*://trail-status.yazdaapp.com/*');
App.accessRule('*://cdn.trail-status.com/*');
App.accessRule('*://cdn-trail-status.yazdaapp.com/*');
App.accessRule('127.0.0.1/0', true);
App.accessRule('10.0.2.2:3000', true);
App.accessRule('*.google.com/*');
App.accessRule('*.googleapis.com/*');
App.accessRule('*.gstatic.com/*');
App.accessRule('*.google-analytics.com/*');
App.accessRule('bnc.lt');
App.accessRule('api.branch.io');

App.configurePlugin('cordova-plugin-facebook4', {
  APP_ID:   '881735831914137',
  APP_NAME: 'Trail Status'
});

App.configurePlugin('appboy-cordova-sdk', {
  APPBOY_PUSH_REGISTRATION_ENABLED: 'true',
  APPBOY_API_KEY: "49644890-c6ad-4dc6-b49d-58da43a8411f",
  APPBOY_GCM_SENDER_ID: "AIzaSyAJIGaFUrjsLYqyOb-gfdkjCik5d41ljOA"
});

App.configurePlugin('cordova-plugin-customurlscheme', {
  'URL_SCHEME': 'yazda-trail-status'
});

App.configurePlugin('branch-cordova-sdk', {
  BRANCH_LIVE_KEY: 'key_live_pkmII4ExwGaBGt8hBW77YnokFtn3fcmy',
  BRANCH_TEST_KEY: 'key_test_hdiOGYussVhzLv6hwY32Xopivwc1lifb',
  URI_SCHEME:      'yazda-trail-status'
});

App.icons({
  iphone:           'resources/Icon-60.png',
  iphone_2x:        'resources/Icon-60@2x.png',
  iphone_3x:        'resources/Icon-60@2x.png',
  ios_settings:     'resources/Icon-Small.png',
  ios_settings_2x:  'resources/Icon-Small@2x.png',
  ios_settings_3x:  'resources/Icon-Small@3x.png',
  ios_spotlight:    'resources/Icon-40.png',
  ios_spotlight_2x: 'resources/Icon-40@2x.png',
  ipad:             'resources/Icon-76.png',
  ipad_2x:          'resources/Icon-76@2x.png',
  android_ldpi:     'resources/drawable-ldpi-icon.png',
  android_mdpi:     'resources/drawable-mdpi-icon.png',
  android_hdpi:     'resources/drawable-hdpi-icon.png',
  android_xhdpi:    'resources/drawable-xhdpi-icon.png'
});

App.launchScreens({
  iphone:                  'resources/Default~iphone.png',
  iphone_2x:               'resources/Default@2x~iphone.png',
  iphone5:                 'resources/Default-568h@2x~iphone.png',
  iphone6:                 'resources/Default-667h.png',
  iphone6p_portrait:       'resources/Default-736h.png',
  iphone6p_landscape:      'resources/Default-Landscape-736h.png',
  //ipad_portrait:           '',
  //ipad_portrait_2x:        '',
  //ipad_landscape:          '',
  //ipad_landscape_2x:       '',
  android_ldpi_portrait:   'resources/drawable-port-ldpi-screen.png',
  android_ldpi_landscape:  'resources/drawable-land-ldpi-screen.png',
  android_mdpi_portrait:   'resources/drawable-port-mdpi-screen.png',
  android_mdpi_landscape:  'resources/drawable-land-mdpi-screen.png',
  android_hdpi_portrait:   'resources/drawable-port-hdpi-screen.png',
  android_hdpi_landscape:  'resources/drawable-land-hdpi-screen.png',
  android_xhdpi_portrait:  'resources/drawable-port-xhdpi-screen.png',
  android_xhdpi_landscape: 'resources/drawable-land-xhdpi-screen.png'
});

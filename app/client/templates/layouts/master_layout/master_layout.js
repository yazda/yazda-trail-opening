Template.MasterLayout.onRendered(function() {
  Meteor.subscribe('images-count');
  Meteor.subscribe('trails-count');
  Meteor.subscribe('services');

  Tracker.autorun(function() {
    if(Session.get('width') >= 768) {
      IonSideMenu.snapper.disable();

      return Session.set('_showWhenSideMenuOpen', true);
    } else {
      IonSideMenu.snapper.enable();
      return Session.set('_showWhenSideMenuOpen', false);
    }
  })
});

Template.MasterLayout.events({
  'click .item.facebook': function() {
    Meteor.connectWith('facebook', function(err) {
      if(!err) {
        Analytics.trackEvent('social', 'connect-facebook');
      }
    });
  },
  'click .item.strava':   function() {
    Meteor.connectWith(Package['selaias:strava'].Strava, function(err) {
      if(!err) {
        Analytics.trackEvent('social', 'connect-strava');
      }
    });
  },
  'click #my-trails':     function() {
    Session.set('searchTrails', false);
    Session.set('searchQuery', undefined);
  },
  'click #feedback':      function() {
    AppboyPlugin.launchFeedback();
  },
  'click #share-rewards': function() {
    Branch
        .createBranchUniversalObject({
          canonicalIdentifier: 'trail-status-rewards',
          title:               'Trail Status',
          contentDescription:  'Trail Status is the only app that alerts you' +
                               ' the moment your favorite MTB trails are' +
                               ' open or closed',
          contentIndexingMode: 'public'
        })
        .then(function(obj) {
          BranchObjManager.setUniversalObject(obj);
          BranchObjManager.showShareSheet({
            "feature": "referral",
            "channel": "in-app",
            "stage":   "in-app"
          }, {}).then(function(res) {
            Analytics.trackEvent('referral', 'refer-friends')
          }, function(err) {
          });
        }, function(err) {
        });
  }
});

Template.MasterLayout.helpers({
  imageCount: function() {
    return Counts.get('images-count');
  },
  trailCount: function() {
    return Counts.get('trails-count');
  },
  year:       function() {
    return moment().year();
  }
});

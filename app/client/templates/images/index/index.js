Template.ImagesIndex.helpers({
  trails: function() {
    return Images.find();
  }
});

Template.ImagesIndex.events({
  'click .approve': function(e) {
    Meteor.call('approveImage', Meteor.userId(), this._id);
  },
  'click .reject':  function(e) {
    S3.delete(this.deleteUrl);
    Meteor.call('rejectImage', Meteor.userId(), this._id);
  }
});

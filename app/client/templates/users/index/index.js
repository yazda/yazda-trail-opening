Template.UsersIndex.events({
  'click table tr': function(e) {
    var target = $(e.target);

    if(target.hasClass('manage-status')) {
      if(Roles.userIsInRole(this._id, ['manage-status'])) {
        Roles.removeUsersFromRoles(this._id, ['manage-status']);
      } else {
        Roles.addUsersToRoles(this._id, ['manage-status']);
      }
    } else if(target.hasClass('manage-users')) {
      //TODO by group
      if(Roles.userIsInRole(this._id, ['manage-users'])) {
        Roles.removeUsersFromRoles(this._id, ['manage-users']);
      } else {
        Roles.addUsersToRoles(this._id, ['manage-users']);
      }
    }
  }

  //TODO add users to admin panel
});

Template.UsersIndex.helpers({
  settings: function() {
    return {
      collection: Meteor.users,
      fields:     [
        {
          key:   'username',
          label: 'Username',
        },
        {
          key:   'manage-users',
          label: 'Manage Users',
          fn:    function(value, object) {
            if(object._id !== Meteor.userId()) {
              var isAdmin = Roles.userIsInRole(object._id, ['manage-users']);

              return new Spacebars.SafeString('<div class="waves-effect waves-light btn manage-users '
                  + (isAdmin ? 'red' : '') + '">'
                  + (isAdmin ? ' <i class="material-icons left">delete</i>Remove Role' : ' <i class="material-icons left">add</i>Add Role')
                  + '</div>');
            }

            return '';
          }
        },
        {
          key:   'manage-status',
          label: 'Manage Status',
          fn:    function(value, object) {
            if(object._id !== Meteor.userId()) {
              var isAdmin = Roles.userIsInRole(object._id, ['manage-status']);

              return new Spacebars.SafeString('<div class="waves-effect waves-light btn ' +
                  'manage-status' + (!isAdmin ? 'red' : '') + '">'
                  + (isAdmin ? ' <i class="material-icons left">delete</i>Remove Role' : ' <i class="material-icons left">add</i>Add Role')
                  + '</div>');
            }

            return '';
          }
        }
      ]
    }
  }
});

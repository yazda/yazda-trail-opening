Template.ionNavBar.events({
  'click #share': function(e) {
    var trail = this;

    BranchObjManager.showShareSheet({
          tags:    [trail.name, trail.status],
          channel: 'mobile',
          feature: 'trail-share'
        }, {})
        .then(function(res) {
          shareThis(trail, res.generatedLink);
        }, function(err) {
          shareThis(trail, 'https://trail-status.com/trails/' + trail._id);
        });
  }
});

Template.TrailsShow.events({
  'click #slider':                    function(e) {
    if(Meteor.userId() && typeof window.plugins.actionsheet !== 'undefined' &&
        typeof MeteorCamera !== 'undefined') {
      var actionsheet = window.plugins.actionsheet;
      var trail = this;

      if(actionsheet) {
        var options = {
          title:                    'Upload a new trail image?',
          buttonLabels:             ['Camera', 'Photo Library'],
          addCancelButtonWithLabel: 'Cancel'
        };
        actionsheet.show(options, function(index) {
          switch(index) {
            case 1:
            case 2:
              MeteorCamera.getPicture({
                width:      750,
                height:     750,
                quality:    80,
                sourceType: index === 1 ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY
              }, function(err, data) {
                if(!err) {
                  S3.upload({
                    files:      [data],
                    path:       'images/trails/' + trail._id,
                    expiration: 120000,
                    encoding:   'base64'
                  }, function(err, result) {
                    if(err) {
                      alert('There was an issue uploading your image. Please try' +
                          ' again later');
                    } else {
                      var image = {
                        deleteUrl: result.url,
                        url:       result.relative_url,
                        trailId:   trail._id,
                        userId:    Meteor.userId()
                      };

                      Meteor.call('addTrailImage', image, function(error, response) {
                        alert('Image uploaded! We will be reviewing this' +
                            ' shortly.');
                      });
                    }
                  });
                }
              });
              break;
            default:
              break;
          }
        });
      } else {
        var redirect = confirm('You need to create an account to receive' +
            ' notifications. Create your account now!');

        if(redirect) {
          Router.go('atSignUp');
        }
      }

    }
  },
  'click .ion-android-star':     function(e) {
    if(!Meteor.userId()) {
      var redirect = confirm('You need to create an account to receive' +
          ' notifications. Create your account now!');

      if(redirect) {
        Router.go('atSignUp');
      }
    } else {
      Meteor.users.update({_id: Meteor.userId()},
          {$pull: {'profile.trails': {$in: [this._id]}}});
      toastr.info('Trail removed from favorites');
      Analytics.trackEvent('trails', 'remove-favorite', this);
    }
  },
  'click .ion-android-star-outline': function(e) {
    if(!Meteor.userId()) {
      var redirect = confirm('You need to create an account to receive' +
          ' notifications. Create your account now!');

      if(redirect) {
        Router.go('atSignUp');
      }
    } else {
      Meteor.users.update({_id: Meteor.userId()},
          {$addToSet: {'profile.trails': this._id}});
      toastr.info('Trail added to favorites');
      Analytics.trackEvent('trails', 'add-favorite', this);
    }
  },
  'click .link':                      function(e) {
    if(Meteor.isCordova) {
      cordova.InAppBrowser.open(this.website, '_system');
    } else {
      window.location = this.website;
    }
  },
  'click .map':                       function(e) {
    if(Meteor.isCordova) {
      cordova.InAppBrowser.open(this.map, '_system');
    } else {
      window.location = this.map;
    }
  },
  'click .change-status':             function(e) {
    var status = 'closed';
    var trail = this;

    if(!Meteor.userId()) {
      var redirect = confirm('You need to create an account to change' +
          ' trail status. Create your account now!');

      if(redirect) {
        Router.go('atSignUp');
      }
    } else {
      var hasPermission = (!this.requiresPermissions || Roles.userIsInRole(Meteor.userId(), 'manage-status', this._id));

      if(hasPermission) {
        IonPopup.show({
          templateName: 'StatusChange',
          buttons:      []
        });
      }
    }
  }
});

Template.TrailsShow.onRendered(function() {
  toastr.options.newestOnTop = false;

  fbq('track', 'ViewContent');
  var image = 'https://cdn.trail-status.com/images/trailstatus-dark.svg';
  var trail = this.data;

  if(trail) {
    if(trail.images && trail.images[0].url) {
      image = 'https://cdn.trail-status.com/' + trail.images[0].url
    } else if(this.images && this.images[0]) {
      image = 'https://cdn.trail-status.com/' + trail.images[0]
    }

    Branch
        .createBranchUniversalObject({
          canonicalIdentifier: trail._id.toString(),
          title:               trail.name,
          contentDescription:  trail.description,
          contentImageUrl:     image,
          contentIndexingMode: 'public',
          contentMetadata:     {
            trail_name: trail.name,
            _id:        trail._id
          }
        })
        .then(function(obj) {
          BranchObjManager.setUniversalObject(obj);
          BranchObjManager.registerView();
          BranchObjManager.listOnSpotlight();
        }, function(err) {
        });
  }

  $('#slider').slick({
    autoplay:      true,
    arrows:        false,
    dots:          false,
    autoplaySpeed: 4000
  });
});

Template.TrailsShow.helpers({
  notificationIcon:      function() {
    if(window.notifiesStatusOfTrail(this._id)) {
      return 'ion-android-star';
    } else {
      return 'ion-android-star-outline';
    }
  },
  lastUpdatedTime:       function() {
    return moment(this.lastUpdated).fromNow();
  },
  statusText: function() {
    return this[this.status.toLowerCase() +'Text'] || this.status;
  },
  statusImageSrc:   function() {
    if(this.status) {
      switch(this.status.toLowerCase()) {
        case 'open':
          return 'open-green.svg';
          break;
        case 'closed':
          return 'closed-red.svg';
          break;
        case 'snow':
          return 'snow-blue.svg';
          break;
        case 'caution':
          return 'caution-yellow.svg';
          break;
        default:
          return 'caution-yellow.svg';
          break;
      }
    } else {
      return 'caution-yellow.svg';
    }
  }
});

Template.ionPopup.events({
  'click .open':        function(e) {
    changeStatus('open');
  },
  'click .caution':     function(e) {
    changeStatus('caution');
  },
  'click .closed':      function(e) {
    changeStatus('closed');
  },
  'click .snow':        function(e) {
    changeStatus('snow');
  },
  'click .close-popup': function(e) {
    IonPopup.close();
  }
});

Template.ionPopup.onRendered(function() {
  var urlArray = window.location.pathname.split('/');
  var _id = urlArray[urlArray.length - 1];
  var trail = Trails.findOne({_id: _id});

  this.$('.open .all-caps').html(trail.openText || 'open');
  this.$('.caution .all-caps').html(trail.cautionText || 'caution');
  this.$('.closed .all-caps').html(trail.closedText || 'closed');
  this.$('.snow .all-caps').html(trail.snowText || 'snowy');
});

function changeStatus(status) {
  var urlArray = window.location.pathname.split('/');
  var _id = urlArray[urlArray.length - 1];
  var trail = Trails.findOne({_id: _id});
  var template = '<div class="list add-comment">' +
      '<div class="item item-header text-center">' +
      '<img src="https://cdn.trail-status.com/images/' + statusImageSrc(trail.status) + '"/>' +
      '<i class="icon ion-android-arrow-forward"></i>' +
      '<img src="https://cdn.trail-status.com/images/' + statusImageSrc(status) + '"/>' +
      '<i class="icon ion-ios-close-empty pull-right close-popup"></i>' +
      '</div>' +
      '<div class="item item-text-wrap text-center">' +
      'Would you like to comment on the trail conditions?' +
      '<textarea id="comment" number="140"></textarea>' +
      //TODO maintanence
      //'<label class="pull-left"><input type="checkbox" id="maintenance" >' +
      //'Maintanence Needed</label>' +
      '</div>' +
      '</div>';

  IonPopup.close();

  setTimeout(function() {
    IonPopup.show({
      template: template,
      buttons:  [
        {
          text:  'NO',
          type:  'button-light button-clear',
          onTap: function(e) {
            updateStatus(trail, status);
          }
        },
        {
          text:  'YES',
          type:  'button-light button-clear',
          onTap: function(e) {
            updateStatus(trail, status, $('#comment').val());
          }
        }
      ]
    });
  }, 500);
}

function updateStatus(trail, status, comment) {
  Meteor
      .call('updateTrailStatus',
          trail._id,
          status,
          comment,
          function(error, response) {
            IonPopup.close();

            if(!error) {
              setTimeout(function() {
                IonPopup.show({
                  templateName: 'TrailShare',
                  buttons:      [
                    {
                      text:  'Done',
                      type:  'button-light button-clear',
                      onTap: function(e) {
                        IonPopup.close();
                      }
                    }
                  ]
                });
              }, 450);

              if(typeof facebookConnectPlugin !== 'undefined') {
                facebookConnectPlugin.logEvent('change-trail-status', {
                  userId:    Meteor.userId(),
                  name:      trail.name,
                  status:    trail.status,
                  updatedAt: new Date()
                }, 0);
              }

              Analytics.trackEvent('trails', 'change-trail-status', trail)
            } else {
              alert(error);
            }
          });
}

function statusImageSrc(status) {
  switch(status.toLowerCase()) {
    case 'open':
      return 'open-green.svg';
      break;
    case 'closed':
      return 'closed-red.svg';
      break;
    case 'snow':
      return 'snow-blue.svg';
      break;
    case 'caution':
      return 'caution-yellow.svg';
      break;
    default:
      return 'caution-yellow.svg';
      break;
  }
}

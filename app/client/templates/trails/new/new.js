Template.ionNavBar.events({
  'click #save-trail': function() {
    var name = $('#trail-name').val();
    var location = $('#trail-location').val();
    var ascent = $('#trail-ascent').val();
    var descent = $('#trail-descent').val();
    var description = $('#trail-description').val();

    Meteor.call('createPendingTrail',
        name,
        location,
        ascent,
        descent,
        description,
        Meteor.userId(),
        function(err) {
          $('#trail-name').val('');
          $('#trail-location').val('');
          $('#trail-ascent').val('');
          $('#trail-descent').val('');
          $('#trail-description').val('');

          alert('Thanks for submitting your trail. We\'ll review and let you' +
              ' know when we\'ve added it to Trail Status');
        });
  }
});

Template.TrailItem.helpers({
  lastUpdatedTime: function() {
    return moment(this.lastUpdated).fromNow();
  },
  trailImage: function() {
    if(this.images && this.images.length > 0) {
      if(this.images[0].url) {
        return this.images[0].url;
      } else {
        return '/' + this.images[0];
      }
    }

    return '';
  },
  statusText: function() {
    return this[this.status.toLowerCase() +'Text'] || this.status;
  },
  statusImageSrc:  function() {
    if(this.status) {
      switch(this.status.toLowerCase()) {
        case 'open':
          return 'open-green.svg';
          break;
        case 'closed':
          return 'closed-red.svg';
          break;
        case 'snow':
          return 'snow-blue.svg';
          break;
        case 'caution':
          return 'caution-yellow.svg';
          break;
        default:
          return 'unknown';
      }
    }
  }
});

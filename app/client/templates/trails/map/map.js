Template.MasterLayout.events({
  'click .navigate': function(e) {
    var trail = this;
    var location = [this.location.coordinates[1], this.location.coordinates[0]];

    if(launchnavigator) {
      launchnavigator
          .navigate(location, null, null, null, {
            disableAutoGeolocation: true
          });
    }
  }
});

Template.TrailsShowMap.onRendered(function() {
  GoogleMaps.load({ v: '3', key: 'AIzaSyCducAa2gKSW_eJTn7gs6w4JddjVRib65c'});
});

Template.TrailsShowMap.onCreated(function() {
  var lat = this.data.location.coordinates[1];
  var lon = this.data.location.coordinates[0];

  GoogleMaps.ready(this.data.name, function(map) {

    var marker = new google.maps.Marker({
      draggable: false,
      position:  new google.maps.LatLng(lat, lon),
      map: map.instance
    });

    marker.addListener('click', function() {
      var location = [lat, lon];

      if(launchnavigator) {
        launchnavigator
            .navigate(location, null, null, null, {
              disableAutoGeolocation: true
            });
      }
    });
  });
});


Template.TrailsShowMap.helpers({
  mapOptions:     function() {
    if(GoogleMaps.loaded()) {
      var lat = this.location.coordinates[1];
      var lon = this.location.coordinates[0];

      return {
        center: new google.maps.LatLng(lat, lon),
        fullscreenControl: false,
        streetViewControl: false,
        zoom: 12
      };
    }
  },
  statusText: function() {
    return this[this.status.toLowerCase() +'Text'] || this.status;
  },
  statusImageSrc: function() {
    if(this.status) {
      switch(this.status.toLowerCase()) {
        case 'open':
          return 'open-green.svg';
          break;
        case 'closed':
          return 'closed-red.svg';
          break;
        case 'snow':
          return 'snow-blue.svg';
          break;
        case 'caution':
          return 'caution-yellow.svg';
          break;
        default:
          return 'caution-yellow.svg';
          break;
      }
    } else {
      return 'caution-yellow.svg';
    }
  }
});

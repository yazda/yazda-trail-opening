var ITEMS_INCREMENT = 10;

Template.ionNavBar.events({
  'click .search':        function() {
    Session.set('searchTrails', true);
  },
  'click .cancel':        function() {
    Session.set('searchTrails', false);
    Session.set('searchQuery', undefined);
  },
  'keyup #trails-search': function(e) {
    var value = e.target.value;

    Session.set('searchQuery', value);
  }
});

Template.TrailsIndex.onRendered(function() {
  var self = this;

  // whenever #showMoreResults becomes visible, retrieve more results
  function showMoreVisible() {
    var threshold, target = $("#showMoreResults");
    if(!target.length) {
      return;
    }

    threshold = self.$('.content').scrollTop() + self.$('.content').height() - target.height();

    if(target.offset().top < threshold) {
      if(!target.data("visible")) {
        // console.log("target became visible (inside viewable area)");
        target.data("visible", true);

        Session.set("queryLimit",
            Session.get("queryLimit") + ITEMS_INCREMENT);
      }
    } else {
      if(target.data("visible")) {
        // console.log("target became invisible (below viewable arae)");
        target.data("visible", false);
      }
    }
  }

// run the above func every time the user scrolls
  self.$('.content').scroll(showMoreVisible);
});

Template.TrailsIndex.onDestroyed(function() {
  this.$('.content').off('scroll');
});

Template.TrailsIndex.onCreated(function() {
  Session.set('queryLimit', ITEMS_INCREMENT);

  if(typeof navigator.geolocation !== 'undefined' &&
      navigator.geolocation.getCurrentPosition) {
    navigator.geolocation.getCurrentPosition(function(position) {
          Session.set('latitude', position.coords.latitude);
          Session.set('longitude', position.coords.longitude);

          if(Meteor.userId()) {
            Meteor.users.update(Meteor.userId(), {
              $set: {
                'profile.userLocation': {
                  type:        'Point',
                  coordinates: [position.coords.longitude, position.coords.latitude]
                }
              }
            });
          }
        },
        function(err) {
          console.log(err);
        },
        {
          maximumAge:         3000,
          timeout:            5000,
          enableHighAccuracy: false
        });
  }

  if(typeof AppRate !== 'undefined') {
    var customLocale = {};
    customLocale.title = "Rate Trail Status";
    customLocale.cancelButtonLabel = "No, Thanks";
    customLocale.laterButtonLabel = "Remind Me Later";
    customLocale.rateButtonLabel = "Rate It Now";
    customLocale.message = "If you enjoy using Trail Status, would you mind" +
        " taking a  moment to rate it? It will help other people find awesome" +
        " trails like you have!";

    AppRate.preferences.openStoreInApp = false;
    AppRate.preferences.storeAppURL.ios = '1046598022';
    AppRate.preferences.storeAppURL.android = 'market://details?id=com.yazda.trail_status_android';
    AppRate.preferences.customLocale = customLocale;
    AppRate.preferences.displayAppName = 'Trail Status';
    AppRate.preferences.usesUntilPrompt = 20;
    AppRate.preferences.promptAgainForEachNewVersion = true;
    AppRate.promptForRating(false);
  }
});

Template.TrailsIndex.helpers({
  moreResults: function() {
    return !(Counts.get('trails-query-count') < Session.get('queryLimit'));
  },
  trails:      function() {
    var lat = Session.get('latitude');
    var lon = Session.get('longitude');
    var query = {};

    if(lat && lon) {
      query = {
        'location': {
          $near: {
            $geometry: {
              type:        'Point',
              coordinates: [lon, lat]
            }
          }
        }
      };
    }

    return Trails.find(query).fetch();
  },
  noTrails:    function() {
    return Trails.find().count() < 1 && !Session.get('searchTrails');
  },
  isSearch:    function() {
    return Session.get('searchTrails');
  },
  value:       function() {
    return Session.get('searchQuery');
  }
});

Template.TrailsIndex.events({
  'click .no-trails-btn': function() {
    Session.set('searchTrails', true);
  }
});

Template.Wizard.events({
  'click .get-started': function() {
    var user = Meteor.user();

    if(user) {
      Meteor.users.update(user._id, {
        $set: {
          'profile.wizard': true
        }
      });

      Router.go('trails');
    } else {
      Router.go('atSignUp');
    }
  }
});

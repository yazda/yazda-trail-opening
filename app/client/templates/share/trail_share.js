Template.ionPopup.events({
  'click .button-facebook': function(e) {
    shareTrail('facebook');
  },
  'click .button-twitter':  function(e) {
    shareTrail('twitter');
  },
  'click .button-message':  function(e) {
    shareTrail('sms');
  },
  'click .button-email':    function(e) {
    shareTrail('email');
  }
});

function shareTrail(type) {
  var urlArray = window.location.pathname.split('/');
  var _id = urlArray[urlArray.length - 1];
  var trail = Trails.findOne({_id: _id});

  BranchObjManager.createLink({
        tags:    [trail.name, trail.status],
        channel: 'mobile',
        feature: 'trail-share'
      }, {})
      .then(function(res) {
        if(typeof res === 'string') {
          res = JSON.parse(res);
        }

        shareToMedium(type, trail, res.generatedLink);
      }, function(err) {
        console.log(err);
        shareToMedium(type, trail, 'https://trail-status.com/trails/' + trail._id);
      });
}

function shareToMedium(type, trail, link) {
  var msg = trail.name + ' is ' +
      (trail[trail.status.toLowerCase() + 'Text'] || trail.status) +
      '. Check out Trail Status and get ' +
      'notifications on trail openings!';

  switch(type) {
    case 'facebook':
      window.plugins.socialsharing.shareViaFacebook(msg, null, link, function(msg) {
      }, function(msg) {
      });
      break;
    case 'twitter':
      window.plugins.socialsharing.shareViaTwitter(msg, null, link, function() {
      }, function() {
      });
      break;
    case 'sms':
      window.plugins.socialsharing.shareViaSMS(msg + '\n\n' + link, function() {
      }, function() {
      });
      break;
    case 'email':
      window.plugins.socialsharing.shareViaEmail('Check out Trail Status and get ' +
          'notifications on trail openings! \n\n' + link,
          trail.name + ' is ' + (trail[trail.status.toLowerCase() + 'Text'] || trail.status), function() {
          }, function() {
          });
      break;
    default:
      break;
  }
}

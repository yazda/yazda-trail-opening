Template.Home.events({
  'click .applestore': function(e) {
    fbq('track','Lead');
    window.open('https://itunes.apple.com/us/app/trail-status/id1046598022?ls=1&mt=8')
  },
  'click .googleplay':function(e) {
    fbq('track','Lead');
    window.open('https://play.google.com/store/apps/details?id=com.yazda.trail_status_android');
  },
  'keyup #trails-search': function(e) {
    var value = e.target.value;

    Session.set('searchQuery', value);
  },
  'click .trail-widget-item': function(e) {
    $('#trails-search').val(this.name);
    Session.set('trail-widget', this);
    Dropdowns.hide('trails-dropdown');
  },
  'click #get-widget-small': function(e) {
    $('#widget-small-js').select();
  },
  'focus #widget-small-js': function(e) {
    $('#widget-small-js').select();
  },
  'click #get-widget-large': function(e) {
    $('#widget-large-js').select();
  },
  'focus #widget-large-js': function(e) {
    $('#widget-large-js').select();
  }
});

Template.Home.helpers({
  trails: function() {
    return Trails.find().fetch();
  },
  selectedId: function() {
    var widget = Session.get('trail-widget');

    if(widget) {
      return widget._id;
    }
  },
  year: function() {
    return moment().year();
  }
});

Template.Home.onRendered(function() {
  Session.set('searchQuery', '');
  Session.set('trail-widget', '');
  /* ------------------------------------------------------------------------ */
  /* PRETTYPHOTO */
  /* ------------------------------------------------------------------------ */
  $("a[data-rel^='prettyPhoto']").prettyPhoto({social_tools: ''});

  /* ------------------------------------------------------------------------ */
  /* FLEXSLIDER */
  /* ------------------------------------------------------------------------ */

  if($('.flexslider').length) {
    $('.flexslider').flexslider({
      animation:      "fade",
      directionNav:   true,
      controlNav:     false,
      pauseOnAction:  true,
      pauseOnHover:   true,
      direction:      "horizontal",
      slideshowSpeed: 5500
    });
  }

});

Template.Home.onCreated(function() {

});

Template.Home.onDestroyed(function() {

});

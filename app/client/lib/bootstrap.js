Meteor.startup(function() {
  if(window.Branch !== undefined) {
    Branch.initSession();

    Tracker.autorun(function() {
      var user = Meteor.user();

      if(Meteor.userId() && user) {
        AppboyPlugin.changeUser(Meteor.userId());
        Branch.setIdentity(Meteor.userId());

        AppboyPlugin.setCustomUserAttribute('has_strava', !!user.services.strava);
        AppboyPlugin.setCustomUserAttribute('has_facebook', !!user.services.facebook);
        AppboyPlugin.setCustomUserAttribute('followed_trails', user.profile.trails);
      }
    });
  }
});

DeepLinkHandler = function(data) {
  if(data._id) {
    Router.go('trails.show', {_id: data._id});
  }
};

BranchObjManager = {
  obj:                {},
  setUniversalObject: function(obj) {
    this.obj = obj;
  },
  registerView:       function() {
    this.obj.registerView();
  },
  createLink:         function(opts, control) {
    return this.obj.generateShortUrl(opts, control);
  },
  showShareSheet:     function(opts, control) {
    return this.obj.showShareSheet(opts, control);
  },
  listOnSpotlight:    function() {
    if(navigator.platform === 'iPhone') {
      return this.obj.listOnSpotlight();
    }
  }
};

Analytics = {
  trackEvent: function(category, action, obj) {
    var opts = obj || {};

    opts.category = category;

    if(typeof Branch !== 'undefined') {
      Branch.userCompletedAction(action, opts);
    }

    if(ga !== 'undefined') {
      ga("send",
          "event",
          category,
          action,
          obj);
    }

    if(typeof AppboyPlugin !== 'undefined') {
      AppboyPlugin.logCustomEvent(action, opts);
    }
  }
};

AutoForm.addHooks(['insertPendingTrailsForm'], {
  onSuccess: function(operation, result, template) {
    Analytics.trackEvent('trails', 'add-new-trail');
  }
});

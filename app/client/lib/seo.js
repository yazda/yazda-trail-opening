Router.plugin('seo', {
  defaults: {
    image:     'https://cdn.trail-status.com/images/trailstatus-fb.png',
    title:   {
      text:      'Trail Status',
      separator: '|',
      suffix:    null
    },
    twitter: {
      card:                  'app',
      site:                  '@trail_status',
      'app:url:iphone':      'yazda-trail-status://',
      'app:url:googleplay':  'yazda-trail-status://',
      'app:id:iphone':       '1046598022',
      'app:id:googleplay':   'com.yazda.trail_status_android',
      'app:name:googleplay': 'Trail Status',
      'app:name:iphone':     'Trail Status'
    },
    og:      {
      site_name: 'Trail Status',
      type:      'website'
    }
  }
});

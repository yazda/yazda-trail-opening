Template.registerHelper('selectedIf', function(val) {
  return val ? 'selected' : '';
});

notifiesStatusOfTrail = function(trailId) {
  var current = Meteor.user();

  if(current && current.profile) {
    if(_.find(current.profile.trails, function(item) {
          return item === trailId;
        })) {
      return true;
    }
  }

  return false;
};

Template.registerHelper('notifiesStatusOfTrail', notifiesStatusOfTrail);

shareThis = function(trail, link) {
  var msg = trail.name + ' is ' + (trail[trail.status.toLowerCase() + 'Text'] || trail.status) +
      '. Check out Trail Status and you can get ' +
      'notifications on trail openings!';

  window
      .plugins
      .socialsharing
      .share(
          msg, // Message
          null, // Subject
          link, // Link
          function() {
            Analytics.trackEvent('trails', 'share-trail-status', trail);
          }
      );
};

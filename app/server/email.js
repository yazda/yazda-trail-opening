Accounts.emailTemplates.siteName = 'Trail Status';
Accounts.emailTemplates.from = 'Trail Status <info@yazdaapp.com>';
Accounts.emailTemplates.verifyEmail.subject = function(user) {
  return 'Welcome to Trail Status - Please Confirm your account';
};
Accounts.emailTemplates.verifyEmail.text = function(user, url) {
  return "Thank you for signing up to Trail Status. Please verify your" +
      " account by clicking the link below \n\n" + url;
};

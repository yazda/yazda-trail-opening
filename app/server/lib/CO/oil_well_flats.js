Meteor.startup(function() {
  var name = 'Oil Well Flats';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Oil Well Flats',
      type:                'Singletrack',
      ascent:              1312,
      descent:             -1312,
      miles:               14.6,
      level:               'Intermediate',
      city:                'Canon City, CO',
      address:             'FS Road 5940, Cañon City, CO 81212',
      location:            {
        type:        'Point',
        coordinates: [-105.218993, 38.538105]
      },
      description:         "Oil Well Flats is a technical flowy trail." +
                           " Expect lots of sharp pointy rocks and cacti on" +
                           " the trail. Much of the trail is short punchy" +
                           " climbs and descents. Island in the sky is a" +
                           " blast either direction. The trails do get" +
                           " fairly busy so try to arrive early to beat the" +
                           " crowds. ",
      website:             'http://www.joinfar.org/trails.html',
      map:                 'http://www.joinfar.org/uploads/1/3/1/6/13163562/oil_well_flats_map_2016_02_19.pdf',
      status:              'caution',
      requiresPermissions: true,
      twitter_tags:        '#FARtrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }

  function checkFAR() {
    var response = Meteor.http.call('GET', 'http://www.joinfar.org/trails.html');
    var trail = Trails.findOne({name: name});
    var status = 'caution';
    var sendPush = false;

    var content = response.content.replace(/(\r\n|\n|\r|\t)/gm, "");
    var m = content.match(/(Oil\sWell\sFlats:).*?(wet|dry)/i);
    var open = m[2].toLowerCase() === 'dry';

    if(open) {
      status = 'open';
      if(trail.status !== 'open') {
        sendPush = true;
        Trails.update({_id: trail._id}, {$set: {status: status}});
      }
    } else {
      if(trail.status !== 'closed') {
        sendPush = true;
        Trails.update({_id: trail._id}, {$set: {status: status}});
      }
    }

    logger.info(trail.name + ' are: ' + status + ' but were: ' + trail.status);

    if(sendPush) {
      var ids = Meteor.users.find(
          {'profile.trails': {$in: [trail._id]}},
          {fields: {_id: '1'}})
          .fetch();
      var userIds = _.pluck(ids, '_id');

      Push.send({
        from:    'Trail Status',
        title:   'Trail Status',
        text:    "Oil Well Flat's status has changed.  The trails are now " + status,
        badge:   1, // TODO better badges
        payload: {
          title: 'Trail Status'
        },
        query:   {userId: {$in: userIds}}
      });
    }
  }

  SyncedCron.add({
    name:     name,
    schedule: function(parser) {
      return parser.text('every 1 hour');
    },
    job:      checkFAR
  });
});

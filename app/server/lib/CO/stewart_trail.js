Meteor.startup(function() {
  var name = 'Stewart Trail at Ridgeline Open Space';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Ridgeline',
      address:             'Coachline Rd, Castle Rock, CO 80109, USA',
      description:         "These soft-surface trails travel through valleys" +
                           " and ridgelines of the 370-acre open space" +
                           " property. Visitors can take in views of the" +
                           " Front Range to the west and the Town below as" +
                           " they traverse open grasslands and dense Gambel" +
                           " oak.",
      city:                'Castle Rock, CO',
      website:             'http://www.crgov.com/Facilities/Facility/Details/Ridgeline-Open-Space-40',
      status:              'open',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'closed', 'caution', 'snow'],
      level:               'Beginner',
      miles:               10,
      ascent:              1076,
      descent:             -1076,
      type:                'Singletrack',
      map:                 'http://www.crgov.com/DocumentCenter/View/296',
      phone:               '303-814-7456',
      email:               'postpartners@CRgov.com',
      requiresPermissions: true,
      location:            {
        type:        'Point',
        coordinates: [
          -104.8949098,
          39.38150949999999
        ]
      },
      twitter_tags:        ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }

  function checkTrailStatus() {
    // TODO specify ENV variables for this
    var response = Meteor.http.call('GET', 'http://gis.crgov.com/arcgis/rest/services/TrailConditions/MapServer/0/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102654');
    var trail = Trails.findOne({name: name});
    var newStatus;
    var comment;

    var status = _.select(JSON.parse(response.content).features, function(obj) {
      return obj.attributes.NAME === 'Stewart Trail Northwest';
    })[0]
        .attributes
        .STATUS
        .toLowerCase();

    if(status === 'poor') {
      newStatus = 'caution';
    } else if(status === 'good' || status === 'open') {
      newStatus = 'open';
    } else if(status === 'closed') {
      newStatus = 'closed';
      comment = 'Closed for an event or maintenance.';
    }

    logger.info(trail.name + ' are: ' + newStatus + ' but were: ' + trail.status);

    if(newStatus && trail.status !== newStatus) {
      var text = trail.shortName + " status changed. now " + (trail[newStatus + 'Text'] || newStatus) + '!';

      Trails.update({_id: trail._id}, {
        $set: {
          status:  newStatus,
          comment: comment
        }
      });

      var ids = Meteor.users.find(
          {'profile.trails': {$in: [trail._id]}},
          {fields: {_id: '1'}})
          .fetch();
      var userIds = _.pluck(ids, '_id');

      Push.send({
        from:    'Trail Status',
        title:   'Trail Status',
        text:    text,
        badge:   1, // TODO better badges
        payload: {
          title: 'Trail Status'
        },
        query:   {userId: {$in: userIds}}
      });
    } else if(!newStatus) {
      throw new Meteor.Error("Couldn't get correct status")
    }
  }

  SyncedCron.add({
    name:     name,
    schedule: function(parser) {
      return parser.text('every 30 minutes');
    },
    job:      checkTrailStatus
  });
});

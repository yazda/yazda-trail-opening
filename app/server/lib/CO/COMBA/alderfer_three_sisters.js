Meteor.startup(function() {
  var name = 'Alderfer Three Sisters';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Alderfer',
      type:                'Singletrack',
      ascent:              4099,
      descent:             -3394,
      miles:               15.3,
      level:               'Beginner/Intermediate',
      city:                'Evergreen, CO',
      address:             '31677 Buffalo Park Rd., Evergreen CO 80439',
      location:            {
        type:        'Point',
        coordinates: [-105.360225, 39.622236]
      },
      description:         'Alderfer Three Sisters has a bit of trail for' +
                           ' everyone. It offers endurance climbing, rocky' +
                           ' descents, and some great flow. It can be rather' +
                           ' busy during the weekends. The network of trails' +
                           ' here are good for a beginner/intermediate' +
                           ' rider. The trails have fantastic vista overlooks.',
      website:             'http://jeffco.us/open-space/parks/alderfer-three-sisters-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Alderfer-Three-Sisters-Park-Documents/Alderfer-Three-Sisters-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Alderfer'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

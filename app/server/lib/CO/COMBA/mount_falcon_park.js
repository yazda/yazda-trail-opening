Meteor.startup(function() {
  var name = 'Mount Falcon Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Mt Falcon',
      type:                'Singletrack',
      ascent:              2327,
      descent:             -4069,
      miles:               12,
      level:               'Intermediate/Advanced',
      city:                'Indian Hills, CO',
      address:             '21004 Mt Falcon Rd, Indian Hills, CO 80454',
      location:            {
        type:        'Point',
        coordinates: [-105.243061, 39.637461]
      },
      description:         "Mount Falcon can be busy during peak hours." +
                           " While the grade isn't neccessarily very steep," +
                           " there are difficult sections of water bars and" +
                           " switchbacks that make the climb challenging." +
                           " Whenever you're done exploring, it's time to" +
                           " head down. Just turn around and get ready for" +
                           " an awesome descent. Turkey Trot is closed to" +
                           " mountain bikers, so heed the well-marked signs" +
                           " on the ascent/descent on Castle.",
      website:             'http://jeffco.us/open-space/parks/mount-falcon-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Mount-Falcon-Park-Documents/Mount-Falcon-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Mt Falcon'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

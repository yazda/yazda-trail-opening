Meteor.startup(function() {
  var name = 'Apex';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Apex',
      type:                'Singletrack',
      ascent:              4170,
      descent:             -1898,
      miles:               9.6,
      level:               'Advanced/Expert',
      city:                'Golden, CO',
      address:             '116 Lookout Mountain Road, Golden, CO',
      location:            {
        type:        'Point',
        coordinates: [-105.209394, 39.715767]
      },
      description:         'Apex Trail and Argos Trail are one-way on odd' +
                           ' calendar dates. You can view the map or the' +
                           ' kiosk at the trailhead. The climb is difficult' +
                           ' and technical. Expect rocks and waterbars to' +
                           ' slow your ascent to the top. Enchanted Forest' +
                           ' and Apex are great for a fun descent; the' +
                           ' descents are technical, so expect steep rocky' +
                           ' sections',
      website:             'http://jeffco.us/open-space/parks/apex-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Apex-Park-Documents/Apex-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

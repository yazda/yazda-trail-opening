Meteor.startup(function() {
  var name = 'Lair o\' the Bear';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Lair o\' the Bear',
      type:                'Singletrack',
      ascent:              453,
      descent:             -448,
      miles:               3.6,
      level:               'Intermediate',
      city:                'Idledale, CO',
      address:             '22600 Colorado 74, Idledale, CO 80453',
      location:            {
        type:        'Point',
        coordinates: [-105.256958, 39.667788]
      },
      description:         "Lair o' the Bear will occasionally close when" +
                           " trails are muddy. The trails can oftentimes be" +
                           " crowded, so be aware of hikers and bikers. The" +
                           " trails are mostly flowy singletrack, though" +
                           " there are a few climbs and descents with fun" +
                           " features. New riders will find this to be a" +
                           " great trail to add to their arsenal. Try to" +
                           " arrive early to avoid the crowds.",
      website:             'http://jeffco.us/open-space/parks/lair-o-bear-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Lair-o-the-Bair-Park-Documents/Lair-o--the-Bear-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Lair o\' the Bear'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

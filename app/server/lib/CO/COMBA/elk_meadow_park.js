Meteor.startup(function() {
  var name = 'Elk Meadow Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Elk Meadow',
      type:                'Singletrack',
      ascent:              4000,
      descent:             -4000,
      miles:               11.8,
      level:               'Intermediate/Advanced',
      city:                'Evergreen, CO',
      address:             '2855 Bergen Peak Drive, Evergreen, CO',
      location:            {
        type:        'Point',
        coordinates: [-105.353193, 39.660244]
      },
      description:         "Elk Meadow Park has different sections for" +
                           " different riders. Beginners will like riding in" +
                           " the meadow, and those looking for something" +
                           " more advanced will enjoy the Bergen Peak" +
                           " sections. The meadow has flat valley riding and" +
                           " singletrack, coupled with some descents. The" +
                           " Bergen Peak sections are technical with" +
                           " switchbacks. Once you're at the top, you'll see" +
                           " amazing views of 14ers in the area.",
      website:             'http://jeffco.us/open-space/parks/elk-meadow-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Elk-Meadow-Park-Documents/Elk-Meadow-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Elk Meadow'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

Meteor.startup(function() {
  var name = 'Chimney Gultch';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Chimney Gultch',
      type:                'Singletrack',
      ascent:              1132,
      descent:             -1132,
      miles:               2.4,
      level:               'Intermediate/Advanced',
      city:                'Golden, CO',
      address:             '910 Colorow Road, Golden CO 80401',
      location:            {
        type:        'Point',
        coordinates: [-105.246440, 39.730766]
      },
      description:         'Chimeny Gultch climbing can be a test of' +
                           ' endurance. There are not many technical' +
                           ' sections other than the water bars. The trail' +
                           ' can be fairly busy like many Front Range' +
                           ' trails. The descent down is a lot of fun. You' +
                           ' can connect the trail to Apex.',
      website:             'http://jeffco.us/open-space/parks/windy-saddle-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Windy-Saddle-Park-Documents/Windy-Saddle-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Chimney Gultch'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

Meteor.startup(function() {
  var name = 'South Valley Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'South Valley',
      type:                'Singletrack',
      ascent:              1072,
      descent:             -1062,
      miles:               8.2,
      level:               'Beginner',
      city:                'Littleton, CO',
      address:             '90 South Valley Rd. Littleton CO 80401',
      location:            {
        type:        'Point',
        coordinates: [-105.154002, 39.564908]
      },
      description:         "South Valley Park is Jefferson County Open Space" +
                           " located in Jefferson County west of Ken Caryl," +
                           " Colorado. The 909-acre (3.68 km2) Front Range" +
                           " park established in 1999 has 8 miles (13 km) of" +
                           " hiking trails. Horse and bicycle travel is" +
                           " allowed on 7 miles (11 km). Facilities include" +
                           " a parking lot, restrooms, picnic sites.",
      website:             'http://jeffco.us/open-space/parks/south-valley-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/South-Valley-Park-Documents/South-Valley-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'White Ranch Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'White Ranch',
      type:                'Singletrack',
      ascent:              2245,
      descent:             -2245,
      miles:               11.7,
      level:               'Advanced',
      city:                'Golden, CO',
      address:             'Belcher Hill Trail, Golden, CO 80403',
      location:            {
        type:        'Point',
        coordinates: [-105.24837, 39.798791]
      },
      description:         "White Ranch offers multi-use trails, geared" +
                           " towards various levels of expertise. You’ll" +
                           " encounter hikers, dogs, bikers, and" +
                           " equestrians. Expect to find both gentle and" +
                           " rugged terrain, so plan your trip ahead of time" +
                           " to match your ability level. That said, it has" +
                           " some of the best singletrack in the area. You" +
                           " can also expect to find rock gardens, technical" +
                           " drops, intense climbs, and switchbacks. Bring" +
                           " your SPF30 - you’ll get hit with a lot of sun" +
                           " on this ride. There are gorgeous views of" +
                           " Denver and the Plains from several spots." +
                           " There’s camping available here as well if you" +
                           " want to make a weekend trip of it.",
      phone:               '(303) 271-5925',
      email:               'jeffcoparks@jeffco.us',
      website:             'http://jeffco.us/open-space/parks/white-ranch-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/White-Ranch-Park-Documents/White-Ranch-Park-Map/',
      status:              'open',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go',
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Matthews Winters / Dakota Ridge';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Matthews Winters',
      type:                'Singletrack',
      ascent:              1414,
      descent:             -2609,
      miles:               9.2,
      level:               'Expert',
      city:                'Golden, CO',
      address:             '1135-1227 County Rd 93, Golden, CO 80401',
      location:            {
        type:        'Point',
        coordinates: [-105.204353, 39.694495]
      },
      description:         "Matthews Winters Park contains fantastic views." +
                           " Take the Red Rocks trail and get a great view" +
                           " of Red Rocks. Morrison Slide has a beautiful" +
                           " vista view of the city. And Dakota Ridge has" +
                           " spectacular views the whole way around. Many of" +
                           " the trails here can be rather technical and" +
                           " rocky. Dakota Ridge is a steep climb with" +
                           " waterbars and several rock features. Once" +
                           " you're on the ridge, expect narrow trails" +
                           " littered with rocks. Bring your technical" +
                           " skills and your endurance for this ride.",
      website:             'http://jeffco.us/open-space/parks/matthews-winters-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Matthews-Winters-Park-Documents/Matthews-Winters-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Matthews Winters'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

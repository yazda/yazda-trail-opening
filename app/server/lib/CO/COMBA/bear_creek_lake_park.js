Meteor.startup(function() {
  var name = 'Bear Creek Lake Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Bear Creek',
      type:                'Singletrack',
      ascent:              576,
      descent:             -561,
      miles:               7.8,
      level:               'Beginner',
      city:                'Morrison, CO',
      address:             'Conoco, 16283 Morrison Road, Morrison, CO 80465',
      location:            {
        type:        'Point',
        coordinates: [-105.184121, 39.653131]
      },
      description:         "Bear Creek Lake Park contains a small network of" +
                           " singletrack trails. Most of the trails are fast" +
                           " rolling, smooth singletrack. The climbing is" +
                           " pretty light here and is great for a beginner," +
                           " with only one real climb being Mt Carbon. ",
      phone:               '(303) 697 6159',
      email:               'bclp@lakewood.org',
      website:             'http://www.lakewood.org/bclp/',
      map:                 'http://www.lakewood.org/Documents/Community_Resources/Parks_Forestry_and_Open_Space/Bear_Creek_Lake_Park/Park_and_Trail_Map.aspx',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#DiscoverAdventure'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Bear Creek'}});
    }
  }
});

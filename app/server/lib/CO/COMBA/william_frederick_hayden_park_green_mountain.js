Meteor.startup(function() {
  var name = 'William Frederick Hayden Park / Green Mountain';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Green Mountain',
      type:                'Singletrack',
      ascent:              1212,
      descent:             -1228,
      miles:               8.3,
      level:               'Intermediate',
      city:                'Morrison, CO',
      address:             '1000 S Rooney Rd, Morrison, CO 80465',
      location:            {
        type:        'Point',
        coordinates: [-105.19239638, 39.69670039]
      },
      description:         "William Frederick Hayden Park, also known as" +
                           " Green Mountain, provides views of Denver," +
                           " Morrison, and Golden from the top. There are" +
                           " several trails that interconnect the park," +
                           " including Rooney Valley Trail and the Green" +
                           " Mountain Loop. The climbs are not terribly" +
                           " challenging, but they can go on for quite" +
                           " awhile. There are some sections that get fairly" +
                           " steep. Green Mountain does get very busy, so" +
                           " keep an eye out for hikers.",
      phone:               '(303) 697 6159',
      email:               'bclp@lakewood.org',
      website:             'http://www.lakewood.org/HaydenPark/',
      map:                 'http://www.lakewood.org/Documents/Community_Resources/Parks_Forestry_and_Open_Space/WFH_Green_Mountain/Trail_Map.aspx',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#DiscoverAdventure'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Green Mountain'}});
    }
  }
});

Meteor.startup(function() {
  var name = 'Deer Creek Canyon Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Deer Creek',
      type:                'Singletrack',
      ascent:              3242,
      descent:             -2245,
      miles:               10.6,
      level:               'Advanced',
      city:                'Littleton, CO',
      address:             '13537, 13691 Grizzly Dr, Littleton, CO 80127',
      location:            {
        type:        'Point',
        coordinates: [-105.151083, 39.544487]
      },
      description:         "Not all trails here are mountain bike friendly;" +
                           " however, the trails are well marked. The trails" +
                           " can be busy during peak hours, so keep an eye" +
                           " out for the occasional hiker or horseback" +
                           " rider. Expect a technical climb that seems to" +
                           " go on for quite awhile. Once you're ready to" +
                           " start descending, you'll be rewarded with a" +
                           " fast, fun descent. ",
      website:             'http://jeffco.us/open-space/parks/deer-creek-canyon-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/Deer-Creek-Canyon-Park-Documents/Deer-Creek-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:            'Drained & Dry',
      cautionText:         'Muddy Patches',
      snowText:            'Snow Covered',
      closedText:          'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'Deer Creek'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText:    'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

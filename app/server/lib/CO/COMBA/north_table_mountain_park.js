Meteor.startup(function() {
  var name = 'North Table Mountain Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'North Table',
      type:                'Singletrack',
      ascent:              1512,
      descent:             -1511,
      miles:               16.2,
      level:               'Beginner/Intermediate',
      city:                'Golden, CO',
      address:             'North Table Loop Trailhead, Golden, Colorado, United States',
      location:            {
        type:        'Point',
        coordinates: [-105.230067, 39.781747]
      },
      description:         "North Table Mountain is a great" +
                           " beginner/intermediate ride. There are season" +
                           " closing on Rim Rock Trail that last through" +
                           " Febuary 1 - July 31 for raptor nesting. You can" +
                           " expect some steep sections and some technical" +
                           " sections. It is a popular hiking spot, so be" +
                           " aware of hikers and other mountain bikers.",
      website:             'http://jeffco.us/open-space/parks/north-table-mountain-park/',
      map:                 'http://jeffco.us/Open-Space/Documents/Park-Documents/North-Table-Mountain-Park-Documents/North-Table-Mountain-Park-Map/',
      status:              'caution',
      requiresPermissions: false,
      twitter_tags:        '#jeffcotrails #DiscoverAdventure',
      openText:    'Drained & Dry',
      cautionText: 'Muddy Patches',
      snowText: 'Snow Covered',
      closedText:  'No Go'
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(!trail.shortName) {
      Trails.update({_id: trail._id}, {$set: {shortName: 'North Table'}});
    }

    if(!trail.openText) {
      Trails.update({_id: trail._id},
          {
            $set: {
              openText:    'Drained & Dry',
              cautionText: 'Muddy Patches',
              snowText: 'Snow Covered',
              closedText:  'No Go'
            }
          });
    }
  }
});

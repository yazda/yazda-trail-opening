Meteor.startup(function() {
  var name = 'Philip S Miller Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'MAC Trails',
      address:             '1375 W Plum Creek Pkwy, Castle Rock, CO 80109, USA',
      description:         "The 7.4-mile native-surface trail network" +
                           " consists of single-track trails divided into" +
                           " four different, but interconnected, loops with" +
                           " 1.2 miles of paved accessible segments.",
      city:                'Castle Rock, CO',
      website:             'http://www.crgov.com/Facilities/Facility/Details/Philip-S-Miller-Park-55',
      status:              'open',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'closed', 'caution', 'snow'],
      level:               'Beginner/Intermediate',
      miles:               7.4,
      ascent:              600,
      descent:             -600,
      type:                'Singletrack',
      map:                 'http://www.crgov.com/DocumentCenter/View/9314',
      requiresPermissions: true,
      location:            {
        type:        'Point',
        coordinates: [
          -104.87857959999997,
          39.3698155
        ]
      },
      twitter_tags:        ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }

  function checkTrailStatus() {
    // TODO specify ENV variables for this
    var response = Meteor.http.call('GET', 'http://gis.crgov.com/arcgis/rest/services/TrailConditions/MapServer/0/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102654');
    var trail = Trails.findOne({name: name});
    var newStatus;
    var comment;

    var status = _.select(JSON.parse(response.content).features, function(obj) {
      return obj.attributes.NAME === 'Stewart Trail Northwest';
    })[0]
        .attributes
        .STATUS
        .toLowerCase();

    if(status === 'poor') {
      newStatus = 'caution';
    } else if(status === 'good' || status === 'open') {
      newStatus = 'open';
    } else if(status === 'closed') {
      newStatus = 'closed';
      comment = 'Closed for an event or maintenance.';
    }

    logger.info(trail.name + ' are: ' + newStatus + ' but were: ' + trail.status);

    if(newStatus && trail.status !== newStatus) {
      var text = trail.shortName + " status changed. now " + (trail[newStatus + 'Text'] || newStatus) + '!';

      Trails.update({_id: trail._id}, {
        $set: {
          status:  newStatus,
          comment: comment
        }
      });

      var ids = Meteor.users.find(
          {'profile.trails': {$in: [trail._id]}},
          {fields: {_id: '1'}})
          .fetch();
      var userIds = _.pluck(ids, '_id');

      Push.send({
        from:    'Trail Status',
        title:   'Trail Status',
        text:    text,
        badge:   1, // TODO better badges
        payload: {
          title: 'Trail Status'
        },
        query:   {userId: {$in: userIds}}
      });
    } else if(!newStatus) {
      throw new Meteor.Error("Couldn't get correct status")
    }
  }

  SyncedCron.add({
    name:     name,
    schedule: function(parser) {
      return parser.text('every 30 minutes');
    },
    job:      checkTrailStatus
  });
});

Meteor.startup(function() {
  var name = 'Hidden Mesa';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Hidden Mesa',
      address:             '4700 CO-86, Castle Rock, CO 80104',
      description:         "Hidden Mesa is a trail that gives you a lot of" +
                           " everything. Expect to come here to practice" +
                           " your technical riding skills. With a choose" +
                           " your own line sort of riding, you can make this" +
                           " ride as difficult or easy as you want. The" +
                           " trail loops like a lolipop and the real fun" +
                           " happens once you've gotten to the top of the" +
                           " climb. Once you're finished trials, or trails" +
                           " riding, head down Rocky Pass for a fun, though" +
                           " short downhill.",
      city:                'Castle Rock, CO 80104',
      website:             'http://www.crgov.com/Facilities/Facility/Details/Hidden-Mesa-Open-Space-3',
      status:              'caution',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'closed', 'caution'],
      level:               'Beginner/Intermediate',
      miles:               7.2,
      ascent:              510,
      descent:             -511,
      type:                'Singletrack',
      map:                 'http://www.crgov.com/DocumentCenter/View/16',
      requiresPermissions: true,
      location:            {
        type:        'Point',
        coordinates: [
          -104.761913,
          39.409369
        ]
      },
      twitter_tags:        '#DiscoverAdventure'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
  function checkTrailStatus() {
    // TODO specify ENV variables for this
    var response = Meteor.http.call('GET', 'http://gis.crgov.com/arcgis/rest/services/TrailConditions/MapServer/0/query?f=json&where=1%3D1&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outFields=*&outSR=102654');
    var trail = Trails.findOne({name: name});
    var newStatus;
    var comment;

    var status = _.select(JSON.parse(response.content).features, function(obj) {
      return obj.attributes.NAME === 'Miller Park Loops';
    })[0]
        .attributes
        .STATUS
        .toLowerCase();

    if(status === 'poor') {
      newStatus = 'caution';
    } else if(status === 'good' || status === 'open') {
      newStatus = 'open';
    } else if(status === 'closed') {
      newStatus = 'closed';
      comment = 'Closed for an event or maintenance.';
    }

    logger.info(trail.name + ' are: ' + newStatus + ' but were: ' + trail.status);

    if(newStatus && trail.status !== newStatus) {
      var text = trail.shortName + " status changed. now " + (trail[newStatus + 'Text'] || newStatus) + '!';

      Trails.update({_id: trail._id}, {
        $set: {
          status:  newStatus,
          comment: comment
        }
      });

      var ids = Meteor.users.find(
          {'profile.trails': {$in: [trail._id]}},
          {fields: {_id: '1'}})
          .fetch();
      var userIds = _.pluck(ids, '_id');

      Push.send({
        from:    'Trail Status',
        title:   'Trail Status',
        text:    text,
        badge:   1, // TODO better badges
        payload: {
          title: 'Trail Status'
        },
        query:   {userId: {$in: userIds}}
      });
    }else if(!newStatus){
      throw new Meteor.Error("Couldn't get correct status")
    }
  }

  SyncedCron.add({
    name:     name,
    schedule: function(parser) {
      return parser.text('every 30 minutes');
    },
    job:      checkTrailStatus
  });
});

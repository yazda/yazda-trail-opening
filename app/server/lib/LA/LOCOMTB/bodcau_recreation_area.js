Meteor.startup(function() {
  var name = 'Bodcau Recreation Area';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Bodcau',
      type:              'Singletrack',
      ascent:            211,
      descent:           -211,
      miles:             7.2,
      level:             'Intermediate',
      city:              'Haughton, LA',
      address:           '1700 Bodcau Dam Rd, Haughton, LA 71037',
      location:          {
        type:        'Point',
        coordinates: [-93.510146, 32.701084]
      },
      description:       "Bodcau Recreation Area houses some of the longest" +
                         " and fastest singletrack in Louisiana. While many" +
                         " trails in the area tend to be flat, Bodcau has" +
                         " some long desccents.",
      website:           "http://www.locomtb.com/trails/bodcau-recreation-area/",
      map:               'http://www.locomtb.com/wordpress/wp-content/uploads/2013/02/Bodcau_Bike_Trail3.pdf',
      status:            'caution',
      twitter_tags:      '#LOCOMTBTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

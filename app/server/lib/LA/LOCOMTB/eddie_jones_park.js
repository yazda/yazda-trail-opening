Meteor.startup(function() {
  var name = 'Eddie Jones Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Monkey Trail',
      type:              'Singletrack',
      ascent:            82,
      descent:           -82,
      miles:             13.5,
      level:             'Beginner/Intermediate',
      city:              'Keithville, LA',
      address:           '8400 Mike Clark Rd, Keithville, LA 71047',
      location:          {
        type:        'Point',
        coordinates: [-93.934529, 32.265477]
      },
      description:       "Eddie D. Jones Park is a multiuse park. There are" +
                         " mountain bike trails, hoseback riding trails, and" +
                         " hiking trails. Expect short steep climbs and" +
                         " quick descents with some really awesome" +
                         " singletrack.",
      website:           "http://www.locomtb.com/trails/monkey-trails/",
      status:            'caution',
      twitter_tags:      '#LOCOMTBTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Acadiana park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Acadiana',
      type:              'Singletrack',
      ascent:            14,
      descent:           -14,
      miles:             4,
      level:             'Beginner/Intermediate',
      city:              'Lafayette, LA',
      address:           'E Alexander St, Lafayette, LA 70501, USA',
      location:          {
        type:        'Point',
        coordinates: [-91.99474420000001, 30.254578]
      },
      description:       "Tight and twisty single track with man made berms and jumps",
      status:            'caution',
      twitter_tags:      '#mtbacadianaTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

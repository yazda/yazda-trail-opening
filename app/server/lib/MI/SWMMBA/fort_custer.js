Meteor.startup(function() {
  var name = 'Fort Custer Recreation Area';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "Fort Custer",
      type:              'Singletrack',
      ascent:            289,
      descent:           -289,
      miles:             8,
      level:             'Intermediate',
      city:              'Augusta, MI',
      address:           '5163 Fort Custer Dr, Augusta, MI 49012, USA',
      location:          {
        type:        'Point',
        coordinates: [-85.34949919999997, 42.326775]
      },
      website:           "http://www.swmmba.com/index.php/trail-systems/fort-custer",
      map:               "http://swmmba.com/images/stories/FCRA%20MASTER%203%20.pdf",
      description:       "The RED LOOP has the most technical sections and" +
                         " biggest climbs of the three loops in the park. The loop has (17)" +
                         " trail markers, and covers a distance of 9 miles. Starting at the" +
                         " trail-head (1), The Trenches introduce you to one of the many unique" +
                         " features of The Fort. The Trenches were dug by solders as part of" +
                         " the training facilities for combat troops when the Fort was a" +
                         " military training base. Winding through this section is like riding" +
                         " a twisting half pipe. Breaking out of The Trenches, you cross an old" +
                         " intersection of the abandon town of Lawler and enter Rocks and Roots" +
                         " (3). This section is flat, but twisty, with - you guessed it - a lot" +
                         " of rocks and roots. You will cross the equestrian trail (4) midway" +
                         " along this section.  Exiting Rocks and Roots takes you into an open" +
                         " meadow and then to The Gravel Pit. Here you will encounter the Camel" +
                         " Humps, a series of short ups and downs with a few twists thrown in." +
                         " The trail then twists along a small stream through dense underbrush." +
                         " The trail come to the water's edge and then makes a sharp left to to" +
                         " climb a steep, twisty section before leveling off to meander through" +
                         " the woods. The trail then breaks out into the open for about 100" +
                         " yards before beginning a switchback climb up Cardiac Bypass. At the" +
                         " top you will cross an old two track (5) and enter a new section" +
                         " \u2013 The Big Meadow. This section starts in the woods, but quickly" +
                         " enters a (you guessed it) big meadow by taking a fast downhill with" +
                         " a wide sweeping turn at the bottom, so keep your hands off the" +
                         " brakes! The trail winds through the meadow for about half a mile," +
                         " then crosses another two track (6) into Frog Holler. Granny's is a" +
                         " challenging Loop which you can bypass by taking a left on the two" +
                         " track at (6) and passing the exit from Granny's (7) to continue on" +
                         " the Red Loop. Back to Frog Holler. This single track runs through" +
                         " the woods and along the edge off two small swales that are home to" +
                         " thousands of frogs \u2013 including tree frogs, leopard frogs, bull" +
                         " frogs and more. This section of trail is a delight in the spring and" +
                         " most of the summer as these thousands of frogs create a chorus of" +
                         " amazing variety and intensity that literally surrounds you from the" +
                         " trees and swales. The trail makes a hard left on a descent and" +
                         " begins a series of short, steep climbs - summiting in an old barn" +
                         " foundation. Another hard left brings you onto the original starting" +
                         " point of Granny's Garden. Granny's winds across ravines that are a" +
                         " part of an ancient glacial moraine, with lots of drops and climbs," +
                         " log crossings and other technical challenges. A final downhill drop" +
                         " exits Granny's (7) and begins a short climb on two track known as" +
                         " D.O.A.. The road levels out at the top and after about 100 yards, it" +
                         " forks to the right (8), back into singletrack for a sweeping cruise" +
                         " through the Sleepy Hollow section. When riding near dusk or later" +
                         " keep an eye out for a headless horseman in this deep and dark" +
                         " section of trail. At (9), a sharp left back onto an old two track" +
                         " (Deliverance) makes for a fast quarter mile to (10). A sharp right" +
                         " for another quarter mile on the straight shot known as Zoom-Zoom" +
                         " which ends in a very, very twisty arrival at (11). Check for" +
                         " oncoming horses and go straight across the equestrian trail. This" +
                         " section begins as a sweeping sections, slightly downhill. Next" +
                         " you'll encounter a skills option built across a large fallen cherry" +
                         " tree. If you haven't ridden it before, take the bypass on the left" +
                         " and check out the backside before riding it. You then begin a" +
                         " descent that becomes faster and steeper as you go, ending with No" +
                         " Fear Chute \u2013 a fast combination of left, right, and left again" +
                         " high banked turns. No Fear ends by crossing the Green loop at The" +
                         " Table Top (12) - Reese Road. Next - The Amusement Park. This was the" +
                         " first section of singletrack developed by and for mountain bikers in" +
                         " 1995 as a demonstration project to convince the DNR that a mountain" +
                         " bike trail system, built and maintained by its users would be a big" +
                         " draw to the Recreation Area. Little did we know. We built it and" +
                         " they came \u2013 doubling the day use of the park within three" +
                         " years. Visits by thousands of bikers from as far away as California," +
                         " Colorado, Ohio, Indiana, Illinois and Wisconsin added to the" +
                         " regulars from Battle Creek, Kalamazoo, Grand Rapids in beyond to" +
                         " make Fort Custer one of the top 50 trail systems in the U.S. The" +
                         " Amusement Park meanders along the south shore of Eagle Lake with" +
                         " great views of the lake and several steep ups and downs including" +
                         " The Demon Drop a steep flagstone paved gully crossing. Next two very" +
                         " close encounters with the lake and a twisting route through Dances" +
                         " With Trees leads to a steep climb up and away from the lake. A" +
                         " shortcut out can be taken by making a right rather than a left at" +
                         " (13). Or, you can continue twisting through the woods to exit the" +
                         " Amusement Park at (14). Turn right onto a long, straight two track" +
                         " downhill leading to The Peninsula (15). The Peninsula was made a" +
                         " hiker-only trail with the adoption of the horse / bike trail" +
                         " separation in the Spring of 2012. Keep to the left at this" +
                         " intersection and ride to the entrance of Crazy Beaver Loop (16)." +
                         " This section loops around a large spring-fed pond with great scenery" +
                         " and swans in the summer. A beaver used to call this pond home, but" +
                         " she had no place to build a dam (no inlet or outlet stream) and in" +
                         " four years she had felled all the poplar trees (preferred food" +
                         " source and building materials) and had literally eaten herself out" +
                         " of house and home. This is Crazy Beaver Loop! There's a couple of" +
                         " climbs and technical downhills and a final ride through another set" +
                         " of technical trenches before exiting where a left at (17) will" +
                         " return you to the trailhead in about a quarter mile.",
      status:            'caution',
      twitter_tags:      '#SWMMBAtrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'South Germantown Bike Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Germantown Bike Park',
      address:             'South Germantown Mountain Bike and BMX Pump Track',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Intermediate',
      city:                'Boyds, MD',
      miles:               1,
      ascent:              10,
      descent:             -10,
      type:                'Dirt Jump',
      description:         'This growing bike park is located along the' +
                           ' Hoyles Mill Trail in the South Gerantown' +
                           ' Recreation Park.  The park features a 5,000' +
                           ' sq/ft pump track and a 10,000+ sq/ft dirt jump' +
                           ' line.  The facility is a partnership between' +
                           ' M-NCPPC & MORE.\n\nRecent additions include' +
                           ' beginner and intermediate lines to the existing' +
                           ' advanced dirt jumps.  The addition of these' +
                           ' lines allows for a more natural progression of' +
                           ' skills for any rider.  Kids as young as 4 have' +
                           ' ridden the facility aboard stryder style bikes.' +
                           '  The bike park also received a  20×20 steel' +
                           ' pavilion early this summer .  It provides' +
                           ' relief from the sun on those hot summer days,' +
                           ' as well as a place for the family to enjoy a' +
                           ' picnic while the kids (of all ages)' +
                           ' ride.\n\nParking for the bike park is at the' +
                           ' Central Park Pond parking lot. Park your car' +
                           ' and ride over the grassy hill under the' +
                           ' power-lines.  The bike park is in the back' +
                           ' towards the tree line. Just look for the huge' +
                           ' jumps and green pavilion.',
      phone:               '(301) 650-4369',
      website:             'http://www.montgomeryparks.org/facilities/south_germantown/#Pumptrack',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.3120632, 39.1475852]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Gambrill State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Gambrill',
      address:           '8602 Gambrill Park Road, Frederick, MD',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '(301) 271 - 7574',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/western/gambrill.aspx',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Advanced',
      city:              'Fredrick, MD',
      miles:             40,
      ascent:            6000,
      descent:           -5921,
      type:              'Singletrack',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/western/gambrill.aspx',
      description:       "Gambrill State Park and it's neighbooring network of" +
                         " trails at Fredrick Watershed are a hidden gem in the " +
                         "DMV area. All trails are open to mountian bikers with the" +
                         " exception of White Oak Trail. You can expect several " +
                         "technical climbs before reaching equally technical " +
                         "descents. All of the trails are marked with blazes. " +
                         "Even still, Gambrill can be difficult to navigate at times. ",
      location:          {
        type:        'Point',
        coordinates: [-77.493483, 39.471488]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


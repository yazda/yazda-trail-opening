Meteor.startup(function() {
  var name = 'Cabin John';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Cabin John',
      type:              'Singletrack',
      ascent:            330,
      descent:           -400,
      miles:             6.3,
      level:             'Beginner/Intermediate',
      city:              'Bethesda, MD',
      address:           '8299 River Road Bethesda, MD 20817',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '(301) 299 0024',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      website:           'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/cabin_john_svu.shtm',
      map:               'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/trailmap_pdfs/cabin_john_svu_trail.pdf',
      description:       "Cabin John starts near the C&O Canal. You can" +
                         " expect a fairly non-technical trail that has" +
                         " short, steep climbs. Some spots of the trail are" +
                         " prone to flooding and stay wet for quite awhile" +
                         " after rainfall.",
      location:          {
        type:        'Point',
        coordinates: [-76.805005, 38.782624]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


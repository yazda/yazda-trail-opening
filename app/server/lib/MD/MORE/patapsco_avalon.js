Meteor.startup(function() {
  var name = 'Patapsco - Avalon';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Avalon',
      address:           '5538-5568 Landing Rd, Elkridge, MD 21075',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscoavalon.aspx',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Beginner/Intermediate',
      city:              'Elkridge, MD',
      miles:             15,
      ascent:            1411,
      descent:           -1411,
      type:              'Singletrack',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscoavalon.aspx',
      map:               'http://dnr2.maryland.gov/publiclands/Documents/Patapsco_Avalonmap.pdf',
      description:       "Patapsco is a great network of trails. This area" +
                         " can often be busy on weekends, but is well worth" +
                         " the trip. Many of the trails are beginner" +
                         " friendly. You can expect some amazing singletrack" +
                         " with a little technical difficulty. There are" +
                         " many great sites to see as you bike through" +
                         " Avalon including Thomas Viaduct, Old Gun Road" +
                         " Stone Arch Bridge, The Swinging Bridge, and" +
                         " Bloede's Dam.",
      location:          {
        type:        'Point',
        coordinates: [-76.752518, 39.218019]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


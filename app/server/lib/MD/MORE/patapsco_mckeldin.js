Meteor.startup(function() {
  var name = 'Patapsco - McKeldin';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'McKeldin',
      address:           '11676 Marriottsville Road Marriottsville MD 21104',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscomckeldin.aspx',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Intermediate/Advanced',
      city:              'Marriottsville, MD',
      miles:             12,
      ascent:            1201,
      descent:           -1174,
      type:              'Singletrack',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscomckeldin.aspx',
      map:               'http://dnr2.maryland.gov/publiclands/Documents/Patapsco_mckeldinmap.pdf',
      description:       "The McKeldin area of Patapsco is more technical" +
                         " than many of the other sections. There are great" +
                         " scenic views in McKeldin - for example, take the" +
                         " Switchback Trail to get a scenic view at Liberty" +
                         " Dam Overlook. ",
      location:          {
        type:        'Point',
        coordinates: [-76.89112, 39.35982]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


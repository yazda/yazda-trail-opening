Meteor.startup(function() {
  var name = 'Seneca Ridge Trail';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'SRT',
      address:             'Seneca Ridge Trail, Darnestown, MD 20878',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Beginner/Intermediate',
      city:                'Darnestown, MD',
      miles:               5.8,
      ascent:              421,
      descent:             -421,
      type:                'Singletrack',
      description:         'The Seneca Ridge Trail (SRT) connects Schaeffer' +
                           ' Farms to Seneca Creek State Park. The terrain' +
                           ' is a mixture of fast flowing single track,' +
                           ' roots, some rocks, and creek crossings (most' +
                           ' bridged). The Seneca ridge trail is well' +
                           ' defined and an out and back. To get into Seneca' +
                           ' Creek State Park from the Riffle Ford' +
                           ' trailhead, cross Riffle Ford Road and pick up' +
                           ' the single track or you can take a left travel' +
                           ' a short distance down the road and the hook a' +
                           ' ride into the doubletrack gravel entrance. From' +
                           ' there you can pick up several trails including' +
                           ' the white which is Mink Hollow and will carry' +
                           ' you into the park proper. CAUTION: The SRT' +
                           ' crosses Germantown Road which is extremely' +
                           ' dangerous with vehicles traveling in excess of' +
                           ' 45 MPH.',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.268688, 39.131344]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

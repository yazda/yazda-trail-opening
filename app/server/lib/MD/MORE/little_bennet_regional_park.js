Meteor.startup(function() {
  var name = 'Little Bennett Regional Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Little Bennett',
      type:              'Singletrack',
      ascent:            540,
      descent:           -540,
      miles:             21,
      level:             'Beginner/Intermediate',
      city:              'Clarksburg, MD',
      address:           '23701 Frederick Rd, Clarksburg, MD 20871',
      location:          {
        type:        'Point',
        coordinates: [-77.290871, 39.246717]
      },
      description:       "This 3,700-acre park offers over 21 miles of" +
                         " natural surface trails in one of our Best Natural" +
                         " Areas" +
                         " http://www.montgomeryparks.org/wildmontgomery/best_natural_areas/bna-index.shtm. " +
                         "The park is mostly forested and lies among" +
                         " the tributaries of Little Bennett Creek. Hawk's" +
                         " Reach Activity Center offers activities, nature" +
                         " films and other routinely scheduled events." +
                         " Little Bennett Regional Park includes 91" +
                         " campsites that are handicapped accessible and an" +
                         " 18-hole golf course. Little Bennett Regional Park" +
                         " is also very fortunate to have an active" +
                         " volunteer force in the 'Friends of Little" +
                         " Bennett' http://www.friendsoflittlebennett.org/.",
      website:           'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/little_bennett.shtm',
      map:               'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/trailmap_pdfs/LittleBennett_trails.pdf',
      status:            'caution',
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Bacon Ridge Natural Area';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Bacon Ridge',
      type:              'Singletrack',
      ascent:            136,
      descent:           -136,
      miles:             3.1,
      level:             'Beginner',
      city:              'Annapolis, MD',
      address:           '1801 Hawkins Rd, Annapolis, MD 21401',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      website:           'http://dnr2.maryland.gov/publiclands/Pages/western/gambrill.aspx',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      website:           'http://www.srlt.org/our-watersheds/bacon-ridge-natural-area',
      description:       "The Bacon Ridge Project is located between" +
                         " Patapsco Valley State Park and Rosaryville. Bacon" +
                         " Ridge is the first natural surface trail project" +
                         " undertaken on property in Anne Arundel County" +
                         " giving outdoor enthusiasts access to singletrack" +
                         " in the Annapolis area. ",
      location:          {
        type:        'Point',
        coordinates: [-76.608175, 39.006209]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


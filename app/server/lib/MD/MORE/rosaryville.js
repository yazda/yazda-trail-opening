Meteor.startup(function() {
  var name = 'Rosaryville State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Rosaryville',
      type:              'Singletrack',
      ascent:            801,
      descent:           -803,
      miles:             11.5,
      level:             'Intermediate',
      city:              'Upper Marlboro, MD',
      address:           '8714 Rosaryville Road, Upper Marlboro, MD 20773',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '1 (800) 830 3974',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/southern/rosaryville.aspx',
      map:               'http://dnr2.maryland.gov/publiclands/Documents/Rosaryville_map.pdf',
      description:       "Rosaryville State Park is a semi-rural park" +
                         " located in Maryland. The park has a 10 mile" +
                         " singlettrack loop that circles the park with an" +
                         " additional inner loop with more technical" +
                         " features. You can expect flowy trail with some" +
                         " steep and sustained climbs. The inner loop has" +
                         " some skinnies, tables and berms to work on" +
                         " technical skills. There is a $3 fee for MD" +
                         " residents and $5 for out of state.",
      location:          {
        type:        'Point',
        coordinates: [-76.805005, 38.782624]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


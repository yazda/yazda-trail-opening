Meteor.startup(function() {
  var name = 'Seneca Bluffs';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Seneca Bluffs',
      address:             '16401 Old River Rd, Poolesville, MD 20837',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Beginner/Intermediate',
      city:                'Poolesville, MD',
      miles:               7.8,
      ascent:              533,
      descent:             -533,
      type:                'Singletrack',
      description:         'The initial 5.3-mile length of the  Seneca' +
                           ' Bluffs trail in Montgomery County, which opened' +
                           ' temporarily for the 2012 MoCo Epic®, was' +
                           ' officially open in late September of 2013. The' +
                           ' trail ran from Black Rock Mill near Schaeffer' +
                           ' Farms to a new trailhead parking lot on' +
                           ' Montevideo Road.\n\nIn early 2014 the trail was' +
                           ' extended 2.5 miles to River Road in' +
                           ' anticipation for the 2014 MoCo Epic. The' +
                           ' additional 2.5 miles extended the trail to the' +
                           ' historic Poole\'s Store near Riley\'s' +
                           ' Lock, allowing riders to connect with other' +
                           ' Montgomery county parks via the C&O Canal' +
                           ' towpath.\n\nThe Seneca Bluffs trail was built' +
                           ' by a coalition of hikers, equestrians, and' +
                           ' mountain bikers in a truly cooperative effort' +
                           ' since 2011.',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.341727, 39.080751]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

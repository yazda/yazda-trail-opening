Meteor.startup(function() {
  var name = 'Schaeffer Farm';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Schaeffer Farm',
      address:             'Kingsview Village Ave, Germantown, MD 20874',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Beginner/Intermediate',
      city:                'Germantown, MD',
      miles:               12.3,
      ascent:              818,
      descent:             -818,
      type:                'Singletrack',
      description:         'Perhaps the most popular trail system in' +
                           ' Montgomery county, Schaeffer Farm offers 15' +
                           ' miles of intermediate riding. Fast, swoopy' +
                           ' trails with few rocks but quite a few roots' +
                           ' traverse a 2000 acre property of which half is' +
                           ' actively farmed in corn and soy beans. Trails' +
                           ' switch frequently from woods to field edge and' +
                           ' two parts of the trail travel through the' +
                           ' middle of the fields, which makes for a special' +
                           ' experience when the corn is high in late' +
                           ' summer. the trail system is well marked with' +
                           ' maps at every intersection, and can be ridden' +
                           ' in many different ways. A relatively new' +
                           ' feature is the "experts only" super' +
                           ' whoops at the southeast corner of the system' +
                           ' (where the Seneca Ridge Trail starts), a set of' +
                           ' about ten whoop-de-do\'s in a deep swale.' +
                           ' This is a great park for beginners, kids,' +
                           ' families and experienced adult riders alike,' +
                           ' since the difficulty increases with speed and' +
                           ' distance.',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.3807017, 39.1430147]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

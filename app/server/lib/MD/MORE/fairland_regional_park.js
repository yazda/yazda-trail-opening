Meteor.startup(function() {
  var name = 'Fairland Regional Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Fairland',
      address:           'Greencastle Trail, Burtonsville, MD 20866',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '(301) 670 - 8080',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      city:              'Burtonsville, MD',
      status:            'caution',
      level:             'Intermediate',
      miles:             8,
      ascent:            653,
      descent:           -651,
      type:              'Singletrack',
      description:       'Fairland is a relatively smooth trail, though there are' +
                         ' some technical sections. The park is pretty easy to' +
                         ' navigate, though bringing a map never hurts! Make sure ' +
                         'you get a chance to ride the rollers for some awesome flow!',
      website:           'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/Fairland.shtm',
      map:               'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/trailmap_pdfs/fairland_trails.pdf',
      location:          {
        type:        'Point',
        coordinates: [-76.929538, 39.0881]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


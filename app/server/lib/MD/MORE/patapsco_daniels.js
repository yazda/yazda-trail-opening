Meteor.startup(function() {
  var name = 'Patapsco - Daniels';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Daniels',
      address:           '2090 Daniels Road Ellicott City MD 21043',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscodaniels.aspx',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Intermediate/Advanced',
      city:              'Ellicott City, MD',
      miles:             19,
      ascent:            2219,
      descent:           -2219,
      type:              'Singletrack',
      website:           'http://dnr2.maryland.gov/publiclands/Pages/central/patapscodaniels.aspx',
      map:               'http://dnr2.maryland.gov/publiclands/Documents/patapsco_danielsmap.pdf',
      description:       "Patapsco Daniels Area is rather undeveloped. Many" +
                         " of the mountain bike trails are unmarked. Expect" +
                         " to find steep climbs, fast descents, and some" +
                         " technical riding. ",
      location:          {
        type:        'Point',
        coordinates: [-76.81557, 39.313665]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


Meteor.startup(function() {
  var name = 'Muddy Branch Greenway';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Muddy Branch',
      address:             '11384-11398 Darnestown Rd, North Potomac, MD 20878',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Beginner/Intermediate',
      city:                'North Potomac, MD',
      miles:               9.2,
      ascent:              395,
      descent:             -395,
      type:                'Singletrack',
      description:         '9 mile of multi-use natural surface trail in' +
                           ' Montgomery County Maryland. The trail traverses' +
                           ' varied terrain and ecosystems including rocky' +
                           ' upland forest, streambanks and meadows, and' +
                           ' passes by rich vernal pools, earthen mill' +
                           ' remnants and the Potomac Horse Center and on' +
                           ' its way from Darnestown Road in Gaithersburg' +
                           ' down to historic Blockhouse Point Conservation' +
                           ' Park. New bridge allows users to cross over to' +
                           ' the C&O Canal trail\n\nGently rolling, but' +
                           ' hilly and steep in sections; includes several' +
                           ' crossings of steep drainages current awaiting' +
                           ' bridges. Road crossings are currently unmarked' +
                           ' but path is blazed with blue markers on trees',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.234804, 39.106799]
      },
      website:             'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/muddy-branch-greenway-trail.shtm',
      map:                 'http://www.montgomeryparks.org/PPSD/ParkTrails/trails_MAPS/trailmap_pdfs/muddy-branch_trailmap_web.pdf',
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

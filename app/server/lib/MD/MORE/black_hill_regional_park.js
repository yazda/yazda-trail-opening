Meteor.startup(function() {
  var name = 'Black Hill Regional Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Black Hill',
      type:              'Singletrack',
      ascent:            772,
      descent:           -770,
      miles:             14.2,
      level:             'Intermediate',
      city:              'Boyds, MD',
      address:           '20926 Lake Ridge Drive, Boyds, MD 20841',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '(301) 528 3490',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      website:           'http://www.montgomeryparks.org/facilities/regional_parks/blackhill/',
      map:               'http://www.montgomeryparks.org/facilities/regional_parks/blackhill/documents/black_hill_regional_map.pdf',
      description:       "Black Hill Regional Park sits near Little Seneca" +
                         " Park. About 4 miles of Black Hill is smooth" +
                         " asphalt, while the rest is natural surface. You" +
                         " can expect a mixture of smooth flowy trails to" +
                         " steep and rocky. There is plenty of parking and" +
                         " Trail Guides are available at the Vistor Center.",
      location:          {
        type:        'Point',
        coordinates: [-77.295245, 39.192751]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


Meteor.startup(function() {
  var name = 'Little Gunpowder';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Little Gunpowder',
      type:              'Singletrack',
      ascent:            1500,
      descent:           -1400,
      miles:             20,
      level:             'Intermediate',
      city:              'Kingsville, MD',
      address:           '12825 Belair Rd, Kingsville, MD 21087, USA',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      website:           'http://www.littlegunpowder.com',
      map:               'http://www.arcgis.com/home/webmap/viewer.html?webmap=f24d753d8d674516a21a6ec07a69d239&extent=-76.4999,39.4172,-76.3204,39.5255',
      description:       "Little Gunpowder State Park is a fun ride with a" +
                         " little bit of everything. You can expect short" +
                         " steep climbs, fast descents and some tech. There" +
                         " are a few stream crossings through the park.",
      location:          {
        type:        'Point',
        coordinates: [39.4745002, -76.40856919999999]
      },
      twitter_tags:      '#GUMBOTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


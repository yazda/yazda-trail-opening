Meteor.startup(function() {
  var name = 'Farmdale Reservoir';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Farmdale',
      type:              'Singletrack',
      ascent:            463,
      descent:           -463,
      miles:             15.7,
      level:             'Intermediate',
      city:              'Washington, IL',
      address:           '810 W Bittersweet Rd, Washington, IL 61571',
      location:          {
        type:        'Point',
        coordinates: [-89.501736, 40.675713]
      },
      description:       "Farmdale Reservoir is managed by the Army Corps of" +
                         " Engineers, and they have allowed the creation of" +
                         " a very nice system of trails. There are" +
                         " approximately 15 miles of trails here, with an" +
                         " excellent mix of fast open stretches and tight," +
                         " twisty singletrack, with many fun gravity dips" +
                         " and ravines. Farmdale also hosts a sweet downhill" +
                         " run, a MTX/dirt jump course, and a freeride/stunt" +
                         " section. Farmdale regularly hosts races in the" +
                         " I74 Race Series, as well as the annual PAMBA fall" +
                         " festival, and was home to the 2007 & 2008 Midwest" +
                         " Mountain Bike Festival.\n\nPark Hours:  Dawn to" +
                         " Dusk\n\n The trails are closed for 24 hours" +
                         " following rain.",
      phone:'(309) 676-4601',
      website:           'http://www.pambamtb.org/farmdale-reservoir',
      map:               'http://www.pambamtb.org/pamba_images/maps/farmdale.jpg',
      status:            'caution',
      twitter_tags:      '#PAMBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

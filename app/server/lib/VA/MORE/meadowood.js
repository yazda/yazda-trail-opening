Meteor.startup(function() {
  var name = 'Meadowood';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Meadowood',
      address:           '10100 Gunston Rd Lorton, VA 22079',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Beginner/Intermediate',
      city:              'Lorton, VA',
      miles:             7.5,
      ascent:            591,
      descent:           -591,
      type:              'Singletrack',
      description:       "Meadowood is a great section of trails for any " +
                         "level of mountain biker. The outer South Branch " +
                         "loop is fairly smooth, though there are some" +
                         " technical sections. Those more expereinced would" +
                         " be happy to find a pumptrack on Yard Sale. The" +
                         " Boss Trail has some exciting features as well." +
                         " While not all trails are open for mountain" +
                         " biking, the trail markers are very helpful.",
      location:          {
        type:        'Point',
        coordinates: [
          -77.209137,
          38.683158
        ]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


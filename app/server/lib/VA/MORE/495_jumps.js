// Meteor.startup(function() {
//   var name = '495 Jumps';
//   var trail = Trails.findOne({name: name});
//
//   if(!trail) {
//     trail = {
//       name:              name,
//       shortName:         '495 Jumps',
//       address:           'Springfield, VA',
//       createdAt:         new Date(),
//       lastUpdated:       new Date(),
//       availableStatuses: ['open', 'caution', 'closed', 'snow'],
//       status:            'open',
//       level:             'Advanced',
//       miles:             2,
//       city:              'Springfield, VA',
//       ascent:            40,
//       descent:           -40,
//       type:              'Dirt Jump',
//       description:       "495 Dirt Jumps are hidden away close to Wakefield" +
//                          " Park. The dirt jumps have two lines, a more" +
//                          " beginner friendly and a more advanced line. If" +
//                          " you ride be sure to help maintain the jumps. If" +
//                          " you uncover them, be sure to re-cover them.",
//       location:          {
//         type:        'Point',
//         coordinates: [-77.224172, 38.817301]
//       },
//       twitter_tags:      '#MORETrails @more_mtb'
//     };
//
//     Trails.insert(trail, function(error, id) {
//     });
//   }
// });

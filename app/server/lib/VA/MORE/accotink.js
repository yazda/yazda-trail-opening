Meteor.startup(function() {
  var name = 'Lake Accotink';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Accotink',
      address:             'Lake Accotink Trail, West Springfield, VA 22152',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      level:               'Intermediate',
      city:                'West Springfield, VA',
      miles:               5.5,
      ascent:              347,
      descent:             -343,
      type:                'Singletrack',
      description:         'Lake Accotink Park is nestled in Springfield on a' +
                           ' 55-acre lake that offers fishing, kayaking, ' +
                           'canoeing, and paddle boating. There are picnic ' +
                           'tables, a carousel, and a snack bar for events. ' +
                           'The mountain bike trails circle the lake, and the ' +
                           'main trail is gravel. Those looking for a more ' +
                           'challenging experience would be well-suited to ' +
                           'discover fun singletrack laden with rocks and ' +
                           'roots. The trail system here can be very ' +
                           'rollercoaster-like with fast descents and short, ' +
                           'lung-busting climbs. The trails do not drain ' +
                           'well, so keep an eye out for muddy spots.',
      website:             'http://www.fairfaxcounty.gov/parks/lake-accotink/',
      map:                 'http://www.fairfaxcounty.gov/parks/lake-accotink/downloads/lapmap2.pdf',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.234633, 38.801158]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

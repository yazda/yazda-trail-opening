Meteor.startup(function() {
  var name = 'Quantico';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Quantico',
      address:           'Bearess Loop, Quantico, VA 22134',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'open',
      level:             'Intermediate',
      miles:             11,
      city:              'Quantico, VA',
      ascent:            1313,
      descent:           -1329,
      type:              'Singletrack',
      website:           'http://www.qmtb.org/index.php/trails/mainside-trails',
      map:               'http://www.quantico.usmc-mccs.org/index.cfm/semper-fit/trail-guide/',
      description:       "The Quantico Trails are located on Marine Corps" +
                         " Base Quantico. In order to access these trails," +
                         " you will need to go through base security." +
                         " Quantico is a very fun mountain bike trail. You" +
                         " can expect technical roots and rocks. These" +
                         " trails have short steep ascents and descents." +
                         " Most of the traffic you will encounter is" +
                         " runners. There are 3 defined loops and the trails" +
                         " are well marked. Green - easiest, Yellow -" +
                         " Intermediate White - Advanced. ",
      location:          {
        type:        'Point',
        coordinates: [-77.313886, 38.530432]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

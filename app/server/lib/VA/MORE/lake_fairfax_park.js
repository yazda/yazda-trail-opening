Meteor.startup(function() {
  var name = 'Lake Fairfax Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      shortName:         'Lake Fairfax',
      address:           '1400 Lake Fairfax Drive Reston, VA 20190 United States',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      phone:             '(703) 471-5414',
      website:           'http://www.fairfaxcounty.gov/parks/lakefairfax/',
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      city:              'Reston, VA',
      level:             'Intermediate',
      miles:             16,
      ascent:            780,
      descent:           -725,
      type:              'Singletrack',
      website:           'http://www.fairfaxcounty.gov/parks/lakefairfax/',
      map:               'http://www.fairfaxcounty.gov/parks/lakefairfax/downloads/trailmap_lakefairfaxpark.pdf',
      description:       "Lake Fairfax is open daily from dawn until dusk. " +
                         "Located in Reston, it contains some great trails " +
                         "for many different levels of riders. Beginners " +
                         "can stick to the main trail, designated by it's " +
                         "wideness that runs from Skatequest to the " +
                         "campgrounds. Those looking for more of a " +
                         "challenge will enjoy the technical rocky and " +
                         "rooty sections of trails. Because the trails run " +
                         "by the creek, there are several sections of " +
                         "stream crossings.",
      location:          {
        type:        'Point',
        coordinates: [-77.317261, 38.966128]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});


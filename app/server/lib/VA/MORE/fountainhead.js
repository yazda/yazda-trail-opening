Meteor.startup(function() {
  var name = 'Fountainhead Regional Park';
  var fountainHead = Trails.findOne({name: name});

  if(!fountainHead) {
    fountainHead = {
      name:                name,
      shortName:           'Fountainhead',
      address:             '10875 Hampton Rd, Fairfax Station, VA 22039',
      description:         'Fountainhead Regional Park is open daily from Dawn to Dusk. We offer a' +
                           ' variety of outdoor recreation opportunities to include Mountain Biking Trails, ' +
                           'Hiking Trails, Equestrian Trails, Picnic Tables, Miniature Golf, Boat Launch ' +
                           'Ramp and Boat Rentals. Keep an eye out for program and event postings as we ' +
                           'lead a variety throughout the season for Children and Adults. If you have any ' +
                           'questions please feel free to give us a call, (703)-250-9124.',
      phone:               '(703) 250-9124',
      city:                'Fairfax Station, VA',
      email:               'fountainhead@nvrpa.org',
      website:             'https://www.nvrpa.org/park/fountainhead/',
      status:              'caution',
      mtbprojectUrl:       '//www.mtbproject.com/widget?v=3&type=trail&id=5398586&x=-8607600&y=4681250&z=6&map=1',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'closed'],
      level:               'Intermediate',
      miles:               11.5,
      ascent:              1258,
      descent:             -1263,
      singletrack:         100,
      type:                'Singletrack',
      images:              ['images/Trails/Fountainhead_Trail.jpg'],
      map:                 'https://www.nvrpa.org/uploads/Files/MBtrail%20map2015.pdf',
      requiresPermissions: true,
      location:            {
        type:        'Point',
        coordinates: [
          -77.3356474,
          38.7210672
        ]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(fountainHead, function(error, id) {
    });
  }

  function checkFacebookStatus() {
    // TODO specify ENV variables for this
    var response = Meteor.http.call('GET', 'https://graph.facebook.com/v2.4/207150099413259/posts?access_token=881735831914137|f9ac5d7d1341a5c269d7b26367d941e1');
    var trail = Trails.findOne({name: 'Fountainhead Regional Park'});
    var latestFacebook = response.data.data[0];

    var open;
    var closed;
    var sendPush = false;
    var status;

      open = latestFacebook.message.toLowerCase().indexOf('open') !== -1;
      closed = latestFacebook.message.toLowerCase().indexOf('closed') !== -1;
      status = open ? 'open' : 'closed';

    logger.info(trail.name + ' are: ' + status + ' but were: ' + trail.status);

    if(open) {
      status = 'open';
      if(trail.status !== 'open') {
        sendPush = true;
        Trails.update({_id: trail._id}, {$set: {status: status}});
      }
    } else if(closed) {
      if(trail.status !== 'closed') {
        sendPush = true;
        Trails.update({_id: trail._id}, {$set: {status: status}});
      }
    }

    if(sendPush) {
      var ids = Meteor.users.find(
          {'profile.trails': {$in: [trail._id]}},
          {fields: {_id: '1'}})
          .fetch();
      var userIds = _.pluck(ids, '_id');

      Push.send({
        from:    'Trail Status',
        title:   'Trail Status',
        text:    "Fountainhead's status has changed.  The trails are now " + status,
        badge:   1, // TODO better badges
        payload: {
          title: 'Trail Status'
        },
        query:   {userId: {$in: userIds}}
      });
    }
  }

  SyncedCron.add({
    name:     name,
    schedule: function(parser) {
      return parser.text('every 15 minutes');
    },
    job:      checkFacebookStatus
  });
});

Meteor.startup(function() {
  var name = 'Laurel Hill';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:                name,
      shortName:           'Laurel Hill',
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      status:              'caution',
      type:                'Singletrack',
      ascent:              347,
      descent:             -343,
      miles:               5.5,
      level:               'Beginner',
      city:                'Lorton, VA',
      address:             'White Orchid Yellow Daisy, Lorton, VA 22079',
      description:         "Laurel Hill is a new addition to MORE's" +
                           " inventory of trails. The trails here are on the" +
                           " grounds of the old Lorton Penitentiary and" +
                           " there are road crossings. Laurel Hill is fast" +
                           " and flowy with few technical sections. The" +
                           " trails do drain very well here and are one of" +
                           " the first to dry out after a storm. ",
      website:             'http://www.fairfaxcounty.gov/parks/laurelhill/',
      requiresPermissions: false,
      location:            {
        type:        'Point',
        coordinates: [-77.238575, 38.709712]
      },
      twitter_tags:        '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

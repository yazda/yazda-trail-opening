Meteor.startup(function() {
  var name = 'Wakefield';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      name:              name,
      address:           '8100 Bradock Road, Annandale, VA 22003 United States',
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      status:            'caution',
      level:             'Beginner/Intermediate',
      miles:             7.3,
      city:              'Annandale, VA',
      ascent:            331,
      descent:           -342,
      type:              'Singletrack',
      website:           'http://www.fairfaxcounty.gov/parks/rec/moore/',
      map:               'http://www.fairfaxcounty.gov/parks/cct/downloads/cct-4-wakefield-trail.pdf',
      description:       "Wakefield, right next door to Accotink Lake has" +
                         " some great singletrack. The park does permit" +
                         " night riding on Monday, Tuesday, and Thursday" +
                         " nights. Wakefield has three distinct trails" +
                         " referenced differently. There is the Creek Trail," +
                         " the Racetrack, and the Bowl. Wakefield is better" +
                         " suited for more intermediate riders. Some of the" +
                         " climbs can be challenging for beginner riders and" +
                         " there are some more technical sections.",
      location:          {
        type:        'Point',
        coordinates: [-77.225203, 38.814852]
      },
      twitter_tags:      '#MORETrails @more_mtb'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

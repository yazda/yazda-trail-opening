Meteor.startup(function() {
  var name = 'Fair Hill Nature Center';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "Fair Hill",
      type:              'Singletrack',
      ascent:            500,
      descent:           -500,
      miles:             21,
      level:             'Beginner/Intermediate',
      city:              'Elkton, MD',
      address:           '2875 Appleton Road Elkton MD 21921',
      location:          {
        type:        'Point',
        coordinates: [-75.822174, 39.714268]
      },
      website:           "http://trailspinners.org/trail-maps/fair-hill",
      map:               "http://trailspinners.org/sites/default/files/uploads/Fair%20Hill%20Trail%20Map%202.png",
      description:       "Miles and miles and miles of trail! lots of cross" +
                         " country and lots of quick singletrack. Bring more" +
                         " water and supplies than you usually do for a" +
                         " smaller park.",
      status:            'caution',
      twitter_tags:      '#TrailspinnersTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

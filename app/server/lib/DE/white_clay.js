Meteor.startup(function() {
  var name = 'White Clay Creek State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "White CLay",
      type:              'Singletrack',
      ascent:            3700,
      descent:           -3700,
      miles:             10,
      level:             'Beginner/Intermediate',
      city:              'Newark, DE',
      address:           'Fairhill School Dr Newark, DE 19711',
      location:          {
        type:        'Point',
        coordinates: [-75.743346, 39.724619]
      },
      description:       "Lots of beginner friendly flow trails. There are" +
                         " some technical trails as well but it never gets very challenging.",
      status:            'caution',
      twitter_tags:      '#TrailspinnersTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

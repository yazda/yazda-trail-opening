Meteor.startup(function() {
  var name = 'Brandywine Creek State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "BWine",
      type:              'Singletrack',
      ascent:            2100,
      descent:           -2100,
      miles:             15,
      level:             'Beginner/Intermediate',
      city:              'Wilmington, DE',
      address:           '320 Rocky Run Parkway Wilmington, DE 19803',
      location:          {
        type:        'Point',
        coordinates: [-75.551428, 39.817908]
      },
      website:           "http://trailspinners.org/trail-maps/brandywine",
      map:               "http://trailspinners.org/sites/default/files/uploads/Brandywine%20Trail%20Map.jpg",
      description:       "Great single track with some challenging and" +
                         " technical terrain as well as long haul cross country.",
      status:            'caution',
      twitter_tags:      '#TrailspinnersTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

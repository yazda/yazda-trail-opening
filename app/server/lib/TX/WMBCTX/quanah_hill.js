Meteor.startup(function() {
  var name = 'Quanah Hill';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:           new Date(),
      lastUpdated:         new Date(),
      availableStatuses:   ['open', 'caution', 'closed', 'snow'],
      name:                name,
      shortName:           'Quanah Hill',
      type:                'Singletrack',
      ascent:              145,
      descent:             -145,
      miles:               3.8,
      level:               'Beginner/Intermediate',
      city:                'Weatherford, TX',
      address:             '810 West Lake Dr., Weatherford, TX',
      location:            {
        type:        'Point',
        coordinates: [-97.6977857, 32.7846657]
      },
      description:         "Physical climb to the top of the property with a" +
                           " nice descent.",
      website:             "http://www.wmbctx.org",
      status:              'caution',
      twitter_tags:        '#WMBCTXTrails',
      requiresPermissions: true
    };

    Trails.insert(trail, function(error, id) {
      Roles.addUsersToRoles('aQEdM3BbtZw6C3Smq', 'manage-status', id);
      Roles.addUsersToRoles('admBoK93TK3ioEzrt', 'manage-status', trail._id);
    });
  }
});

Meteor.startup(function() {
  var name = 'Salado Creek Greenway';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Salado',
      type:              'Singletrack',
      ascent:            855,
      descent:           -855,
      miles:             20,
      level:             'Intermediate',
      city:              'San Antonio, TX',
      address:           '1021 Voelcker Lane, San Antonio, TX, 78248, us',
      location:          {
        type:        'Point',
        coordinates: [-98.526856, 29.563395]
      },
      description:       "Salado Creek Greenway is mostly singletrack with" +
                         " some doubletrack thrown in. There are plenty of" +
                         " off-shoot trails to ride on. There are some cool" +
                         " features including a see-saw. The trails aren't" +
                         " very technical, but there are some loose exposed" +
                         " rocky sections. ",
      phone:             '(210) 207-2879',
      website:           "http://www.mountainbiketx.com/texas/trails/region_5/saladocreek.php",
      map:               'http://www.mountainbiketx.com/downloads/texas/maps/Salado_Creek.pdf',
      status:            'caution',
      twitter_tags:      '#STORMTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

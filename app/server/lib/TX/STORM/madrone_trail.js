Meteor.startup(function() {
  var name = 'Madrone Trail';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Madrone',
      type:              'Singletrack',
      ascent:            200,
      descent:           -200,
      miles:             6,
      level:             'Intermediate',
      city:              'Canyon Lake, TX',
      address:           'Canyon park, Canyon Lake, TX',
      location:          {
        type:        'Point',
        coordinates: [-98.232107, 29.898326]
      },
      description:       "The team that did the trails here really did a" +
                         " great job. The trails are in excellent shape and" +
                         " there is a wide variety of trails available. The" +
                         " skill levels of the trails are more middle of the" +
                         " road with a few technical spots, but overall it" +
                         " makes for a fast ride and lots of quick twists" +
                         " and turns. The trails are very twisty and tightly" +
                         " packed into a small amount of room. The" +
                         " trailbuilders did a great job of laying out the" +
                         " trails so you continually criss-cross back and" +
                         " forth, but the ride never gets boring.",
      website:           "http://austinbike.com/index.php/south-of-austin/24-madrone",
      map:               'http://www.swf-wc.usace.army.mil/canyon/Maps/Madrone_Map2011.pdf',
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  } else {
    if(trail.twitter_tags === '') {
      Trails.update({_id: trail._id}, {
        $set: { twitter_tags: '#STORMTrails'}
      });
    }
  }
});

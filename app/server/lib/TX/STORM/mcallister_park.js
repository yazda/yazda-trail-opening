Meteor.startup(function() {
  var name = 'McAllister Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'McAllister',
      type:              'Singletrack',
      ascent:            913,
      descent:           -920,
      miles:             26.1,
      level:             'Intermediate',
      city:              'San Antonio, TX',
      address:           'Buckhorn Rd, San Antonio, TX 78247',
      location:          {
        type:        'Point',
        coordinates: [-98.454363, 29.559567]
      },
      description:       "McAllister offers 10 miles of wildly rocky and" +
                         " fast trail, especially the upper loop with a view" +
                         " of the airport. It’s free to park here, so it’s a" +
                         " great spot to bring new riders who want to try" +
                         " out some decent hills and rocky areas. There are" +
                         " bathroom facilities and pavilions for BBQs and" +
                         " gathering, in case the new riders are looking to" +
                         " celebrate after conquering their first rock" +
                         " garden. Note that the weekends tend to get busy -" +
                         " there are running events, soccer games, and bike" +
                         " races; so go during the week if you can.",
      website:           'http://www.sanantonio.gov/ParksAndRec/ParksFacilities/AllParksFacilities/ParksFacilitiesDetails/TabId/3354/ArtMID/14820/ArticleID/2578/McAllister-Park.aspx?Park=141&Facility=',
      map:               'http://www.mountainbiketx.com/downloads/texas/maps/McAllister.pdf',
      status:            'caution',
      twitter_tags:      '#STORMTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

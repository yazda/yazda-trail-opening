Meteor.startup(function() {
  var name = 'Millican Reserve Trail';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Millican',
      type:              'Singletrack',
      ascent:            330,
      descent:           -330,
      miles:             13.5,
      level:             'Beginner/Intermediate',
      city:              'Millican, TX',
      address:           '19851 FM 2154, Millican, TX 77866 United States',
      location:          {
        type:        'Point',
        coordinates: [-96.247152, 30.479494]
      },
      description:       "Only BVMBA members who have signed the Millican" +
                         " Waiver are allowed to ride here. ",
      website:           'https://brazosvalleymountainbikeassociation.wordpress.com/',
      status:            'caution',
      twitter_tags:      '#BVMBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

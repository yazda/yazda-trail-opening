Meteor.startup(function() {
  var name = 'Lake Bryan West Loop';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Lake Bryan - West',
      type:              'Singletrack',
      ascent:            331,
      descent:           -331,
      miles:             9.3,
      level:             'Beginner/Intermediate',
      city:              'Bryan, TX',
      address:           'P4-746, Bryan, TX 77807',
      location:          {
        type:        'Point',
        coordinates: [-96.466351, 30.704684]
      },
      description:       "The land the trails are on (and the rest of Lake" +
                         " Bryan) is owned by BTU for the primary purpose of" +
                         " generating electricity. If trail erosion in any" +
                         " way damages any part of the levee or surrounding" +
                         " area, BTU will be forced to close the trail" +
                         " system to protect their investment in the power" +
                         " plant.\n\nPlease help us keep the trails open" +
                         " rideable, and do not ride the trails when they" +
                         " are wet. Lake Bryan is multiuse, so you can" +
                         " expect mountain bikers, hikers and other trail" +
                         " users. The trails are mostly flat, but flowy. The" +
                         " trails are more out and back than typical loop" +
                         " trails, so be prepared to ride back and forth.",
      website:           'https://brazosvalleymountainbikeassociation.wordpress.com/lake-bryan-trail-maps/',
      map:               'https://brazosvalleymountainbikeassociation.files.wordpress.com/2015/12/lakebryan_2015_color_34x44.pdf',
      status:            'caution',
      twitter_tags:      '#BVMBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Mission - RGV';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "Mission",
      type:              'Singletrack',
      ascent:            15,
      descent:           -15,
      miles:             14,
      level:             'Beginner/Intermediate',
      city:              'Mission, TX',
      address:           '1801 S Conway Ave, Mission, TX 78572, USA',
      location:          {
        type:        'Point',
        coordinates: [-98.33139879999999, 26.1829107]
      },
      description:       "Mission Trails feature 14 miles of native single" +
                         " track and double track trail for hiking and" +
                         " biking.  Most is flat with occasional 5-15ft" +
                         " elevation changes.  Have sealant in your tires as" +
                         " cactus and thorns abound.",
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

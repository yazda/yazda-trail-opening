Meteor.startup(function() {
  var name = 'Harlingen Thicket - RGV';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "The Thicket",
      type:              'Singletrack',
      ascent:            25,
      descent:           -25,
      miles:             2,
      level:             'Beginner/Intermediate',
      city:              'Harlingen, TX',
      address:           'E Taft Ave, Harlingen, TX 78550, USA',
      location:          {
        type:        'Point',
        coordinates: [-97.68525820000002, 26.178764]
      },
      description:       "Short but sweet.  The Harlingen thicket trails may" +
                         " be short but offer a challenging combination of" +
                         " elevation change and natural terrain for honing" +
                         " one's MTN biking skills.  Not recommended for" +
                         " very new bikers due to a few challenging" +
                         " elevation features.  Surrounding asphalt trails" +
                         " adjacent to thicket make for additional flat and" +
                         " easy miles that connect to Mckelvey park and" +
                         " Arroyo Park (additional parking).",
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

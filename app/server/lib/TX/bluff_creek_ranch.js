Meteor.startup(function() {
  var name = 'Bluff Creek Ranch';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Bluff Creek',
      type:              'Singletrack',
      ascent:            425,
      descent:           -425,
      miles:             11,
      level:             'Beginner/Intermediate',
      city:              'Warda, TX',
      address:           '537 Owl Creek Rd, Warda, TX 78960',
      location:          {
        type:        'Point',
        coordinates: [-96.908850, 30.057924]
      },
      description:       "Bluff Creek Ranch is about an hour to an hour and" +
                         " a quarter from Austin and offers a wide variety" +
                         " of trails. Bike rentals are also available on" +
                         " site, so if you are riding with an out of towner," +
                         " you can have a bike waiting for them when you" +
                         " arrive. The owners are very friendly and there is" +
                         " bike repair on site and they can really come" +
                         " through in a pinch. Trails vary and are all well" +
                         " maintained. Imagine the best flowy singletrack of" +
                         " Rocky Hill but without a Fat Chuck or Ike's Peak." +
                         " No jeep trails either for the most part. From a" +
                         " challenge standpoint, it's a little less" +
                         " challenging than Rocky Hill Ranch but still" +
                         " offers a wide variety of diversions on the trail" +
                         " to keep you interested. If you are taking an" +
                         " inexperienced person out, this is a great place," +
                         " and if you want to just ride sweet singltrack as" +
                         " fast as you can, this is also the place for you.",
      website:           "http://austinbike.com/index.php/east-of-austin/29-warda",
      map:               'http://www.bcrwarda.com/maps.html',
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

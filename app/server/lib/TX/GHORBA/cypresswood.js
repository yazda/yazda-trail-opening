Meteor.startup(function() {
  var name = 'Cypresswood - Collins Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Cypresswood',
      type:              'Singletrack',
      ascent:            25,
      descent:           -28,
      miles:             21,
      level:             'Beginner/Intermediate',
      city:              'Spring, TX',
      address:           '6701-6727 Cypresswood Dr, Spring, TX 77379',
      location:          {
        type:        'Point',
        coordinates: [-95.509439, 30.013718]
      },
      description:       "Cypresswood Trails are one of the jewels of" +
                         " Houston's trail system. An eight mile" +
                         " out-and-back consisting of numerous stacked loops" +
                         " give riders a well constructed and maintain" +
                         " trailed system in Houston's northern suburbs." +
                         " They typically drain faster than most of the" +
                         " other trails in Houston, snaking along the banks" +
                         " of Cypress Creek from Collins Park to W. Strack" +
                         " Road. Cypresswood is a favorite of riders of many" +
                         " skill levels. Trail markers are well maintained" +
                         " and finding your way on the trails is fairly" +
                         " easy. Difficult obstacles and their bypasses are" +
                         " all clearly marked. Contact the Trail Steward" +
                         " http://ghorba.org/contact/cypresswood-collins-park",
      website:           'http://ghorba.org/trails/cypresswood-collins-park',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

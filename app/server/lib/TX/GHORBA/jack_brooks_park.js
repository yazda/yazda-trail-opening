Meteor.startup(function() {
  var name = 'Jack Brooks Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Jack Brooks',
      type:              'Singletrack',
      ascent:            17,
      descent:           -15,
      miles:             6.5,
      level:             'Advanced/Expert',
      city:              'Hitchcock, TX',
      address:           '10 Jack Brooks Rd, Hitchcock, TX 77563',
      location:          {
        type:        'Point',
        coordinates: [-95.041106,29.362800 ]
      },
      description:       "Jack Brooks Park offers some of the most" +
                         " challenging trails in the Houston Area. For you" +
                         " city and northern Houston dwellers, it’s worth a" +
                         " weekend drive down to Hitchcock. The maintenance" +
                         " crew led by the most excellent trail steward," +
                         " Booker, does an incredible job of maintaining the" +
                         " trail and adding Technical Trail Features (TTFs)" +
                         " that make for a fun ride, in full cooperation" +
                         " with the the Galveston Country Parks department." +
                         " Trails are one way please follow arrows & try not" +
                         " to go backwards. Contact the Trail Steward" +
                         " http://ghorba.org/contact/jack-brooks-park",
      website:           'http://ghorba.org/trails/jack-brooks-park',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/latest.jpg',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

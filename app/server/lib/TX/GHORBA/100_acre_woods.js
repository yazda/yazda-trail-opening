Meteor.startup(function() {
  var name = '100 Acre Woods - Cypress Creek';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         '100 Acre Woods',
      type:              'Singletrack',
      ascent:            48,
      descent:           -48,
      miles:             9.4,
      level:             'Intermediate',
      city:              'Houston, TX',
      address:           'Cypresswood Bike Path, Houston, TX 77070',
      location:          {
        type:        'Point',
        coordinates: [-95.577607, 29.983887]
      },
      description:       "Located in northwest Harris County, the 100-Acre" +
                         " Wood Preserve contains several unique forest" +
                         " communities, including an upland forest with" +
                         " remnant mature post oaks, and a forested wetland" +
                         " with overcup oak. The preserve contains 2 miles" +
                         " of natural surface hiking and biking trails, and" +
                         " is popular with local mountain bikers, hikers," +
                         " dog walkers, bird watchers and nature lovers" +
                         " alike. Just south and under the underpass it" +
                         " links with the Cypress Creek Trails and 8 more" +
                         " miles of single track with technical features." +
                         " Contact the Trail Steward" +
                         " http://ghorba.org/contact/100-acre-woods",
      website:           'http://ghorba.org/trails/100-acre-woods',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/100-acre-wood-mtb-map.jpg',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

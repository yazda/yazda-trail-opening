Meteor.startup(function() {
  var name = 'Memorial Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Memorial Park',
      type:              'Singletrack',
      ascent:            25,
      descent:           -25,
      miles:             6,
      level:             'Beginner/Intermediate',
      city:              'Houston, TX',
      address:           'Purple Trail (Easy), Houston, TX 77007',
      location:          {
        type:        'Point',
        coordinates: [-95.445100,29.765200 ]
      },
      description:       "Memorial Park offers a convenient escape from the" +
                         " concrete jungle of Houston with seven miles of" +
                         " dirt trails. Trails are marked and named by their" +
                         " “Color” of trail marker, with the exception of" +
                         " “The Triangle” Green: The longest single track" +
                         " trail in MP is an out and back trail on the south" +
                         " east side of the park. Yellow: This trail" +
                         " features some of the best and fastest turns in" +
                         " the park and crosses a few gully’s which give the" +
                         " trail some elevation change. Red: What this trail" +
                         " lacks in elevation change it makes up for in" +
                         " fast, flowy and tight single track turns. Blue:" +
                         " Most technical of the core trails, this single" +
                         " track trail features trail “exposure” and many" +
                         " exposed roots which create small ledge drops." +
                         " Orange: Fast and flowy trail similar to yellow" +
                         " with a few fun “G-out” bridges, hair pin turns," +
                         " and a few drops. The Triangle: An often missed" +
                         " and lower traffic trail on the west side of the" +
                         " park. This is the most difficult trail in the" +
                         " park and features a continuously rolling trail," +
                         " fast turns. Purple: This is the “dirt road” that" +
                         " forms a loop through the mountain bike trail" +
                         " area. Mostly flat except for its crossing of a" +
                         " drainage near Buffalo Bayou known as “Call Out”" +
                         " where riders will rapidly descend thirty feet and" +
                         " then immediately climb out of the drainage." +
                         " Please use caution as these are two-way multi use" +
                         " trails and can be very busy on the weekends." +
                         " Please be courteous to other trail users and" +
                         " remember that cyclists yield to both foot traffic" +
                         " and horses. Make your presence known when" +
                         " approaching blind corners and always maintain" +
                         " control of your speed. Contact the Trail Steward" +
                         " http://ghorba.org/contact/memorial-park",
      website:           'http://ghorba.org/trails/memorial-park',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

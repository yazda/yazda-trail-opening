Meteor.startup(function() {
  var name = 'Flintridge';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Flintridge',
      type:              'Singletrack',
      ascent:            10,
      descent:           -10,
      miles:             5,
      level:             'Beginner',
      city:              'The Woodlands, TX',
      address:           'Flintridge, The Woodlands, TX 77381',
      location:          {
        type:        'Point',
        coordinates: [-95.517600, 30.162950 ]
      },
      description:       "This 5 mile trail is located near the Woodlands." +
                         " It’s part of the George Mitchell Preserve (GMP)" +
                         " within Spring Creek Greenway and is a host trail" +
                         " of GHORBA’s annual Short Track Race Series. The" +
                         " inner loop is great single track; tight and" +
                         " twisty. If you want to improve your handling" +
                         " skills this is a great place to work on them. The" +
                         " inner loop takes about twenty minutes to ride and" +
                         " when you turn and ride it the opposite direction" +
                         " it is a new course. Do that twice and if you have" +
                         " not gotten a good workout you weren’t riding hard" +
                         " enough. A couple of laps around the main trail" +
                         " will get your heart rate up too, but keep in mind" +
                         " cyclists are not the only trail users." +
                         " http://ghorba.org/contact/flintridge",
      website:           'http://ghorba.org/trails/flintridge',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

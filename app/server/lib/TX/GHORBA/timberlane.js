Meteor.startup(function() {
  var name = 'Timberlane';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Timberlane',
      type:              'Singletrack',
      ascent:            10,
      descent:           -10,
      miles:             4,
      level:             'Beginner',
      city:              'Spring, TX',
      address:           '2699 Ciderwood Dr Spring, TX 77373',
      location:          {
        type:        'Point',
        coordinates: [-95.388897, 30.044240]
      },
      description:       "Timberlane features four miles of mostly natural" +
                         " surface single track with the occasional" +
                         " crushed granite double track. The trails are" +
                         " similar in surface and layout to nearby" +
                         " Cypresswood as they wind through the forests" +
                         " along the bank of Cypress Creek. Timberlane" +
                         " does not feature technical features such as" +
                         " drops or jumps, the fun of this trail comes" +
                         " from the well laid out, rolling, twisty, and" +
                         " flowy single track. This is a multi use trail," +
                         " please use caution for other users. Contact the" +
                         " Trail Steward" +
                         " http://ghorba.org/contact/timberlane",
      website:           'http://ghorba.org/trails/timberlane',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

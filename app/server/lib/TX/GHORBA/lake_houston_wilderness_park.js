Meteor.startup(function() {
  var name = 'Lake Houston Wilderness Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Lake Houston',
      type:              'Singletrack',
      ascent:            50,
      descent:           -60,
      miles:             10,
      level:             'Beginner',
      city:              'New Caney, TX',
      address:           '25840 FM 1485 77357 New Caney, TX',
      location:          {
        type:        'Point',
        coordinates: [-95.167707, 30.147996 ]
      },
      description:       "The trails in Lake Houston Wilderness park offer" +
                         " 10 miles of trails for cross country mountain" +
                         " bikers to hone their skills, work on endurance," +
                         " or just spin along all the while through some of" +
                         " south east texas more unique terrain. There are" +
                         " currently three good singletrack trails in this" +
                         " park. All of which continue to improve on flow." +
                         " These can be connected by various gravel roads" +
                         " and paths to make longer routes. Maps available" +
                         " at entry station but need improving. Contact the" +
                         " Steward" +
                         " http://ghorba.org/contact/lake-houston-wilderness-park",
      website:           'http://ghorba.org/trails/lake-houston-wilderness-park',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/trail-map-lhwp2_0.jpg',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

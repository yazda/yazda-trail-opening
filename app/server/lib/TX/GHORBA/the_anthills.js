Meteor.startup(function() {
  var name = 'The Anthills - Terry Hershey Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'The Anthills',
      type:              'Singletrack',
      ascent:            40,
      descent:           -35,
      miles:             9.3,
      level:             'Intermediate',
      city:              'Houston, TX',
      address:           'Terry Hershey Park Parking Lot, Houston, TX',
      location:          {
        type:        'Point',
        coordinates: [-95.605000, 29.761300]
      },
      description:       "This is a wooded intermediate to advanced single" +
                         " track trail system along the south bank of" +
                         " Buffalo Bayou between Eldridge and Wilcrest" +
                         " streets. This is a fun twisty single track ride" +
                         " with lots of ups and downs over trails that" +
                         " crisscross between the lower river bank and the" +
                         " top of the bank (as much as 40' of elevation" +
                         " change in some areas). There are several" +
                         " seriously steep but short climbs in the middle" +
                         " trails. The trails along the lower river bank" +
                         " and up on top are easier. The middle trails get" +
                         " progressively more difficult as you make your" +
                         " way toward Wilcrest. Due to the proximity to" +
                         " the river the undergrowth grows quickly so" +
                         " visibility around the next corner may not be" +
                         " the best. Holler out often to warn others of" +
                         " your presence. Contact the Trail Steward" +
                         " http://ghorba.org/contact/anthills-terry-hershey-park",
      website:           'http://ghorba.org/trails/anthills-terry-hershey-park',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/anthills11x17-r0.jpg',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

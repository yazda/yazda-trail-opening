Meteor.startup(function() {
  var name = 'Huntsville State Park Trails';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Huntsville',
      type:              'Singletrack',
      ascent:            320,
      descent:           -320,
      miles:             15.5,
      level:             'Beginner/Intermediate',
      city:              'New Waverly, TX',
      address:           'State Park Road 40, New Waverly, TX 77358',
      location:          {
        type:        'Point',
        coordinates: [-95.534300, 30.614000  ]
      },
      description:       "North of Houston on the southern edge of the city" +
                         " of Huntsville is a great place to ride. With 7.5" +
                         " miles of single track trail and 8 more miles of" +
                         " assorted double track natural surface trail" +
                         " combined with the most elevation changes of any" +
                         " local trails, Huntsville is a great challenge for" +
                         " riders of all ability levels and much like Double" +
                         " Lake, the challenge increases the faster you" +
                         " ride. The trails are primarily single track set" +
                         " amongst the East Texas forest and have a surface" +
                         " of hard packed dirt with areas of sand in corners" +
                         " and drainages. Roots are regularly exposed on the" +
                         " trail and can be very slick when wet. Remember," +
                         " this is a multi use trail, please use caution for" +
                         " other users and opposite direction traffic." +
                         " Contact the Trail Steward" +
                         " http://ghorba.org/contact/huntsville-state-park",
      website:           'http://ghorba.org/trails/huntsville-state-park',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

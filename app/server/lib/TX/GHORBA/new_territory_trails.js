Meteor.startup(function() {
  var name = 'New Territory Trails';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'NTMTB',
      type:              'Singletrack',
      ascent:            20,
      descent:           -20,
      miles:             13,
      level:             'Beginner/Intermediate',
      city:              'Sugar Land, TX',
      address:           '3315 Wimberly Canyon Dr, Sugar Land, TX 77479',
      location:          {
        type:        'Point',
        coordinates: [-95.693028, 29.565656]
      },
      description:       "Located in Sugar Land, TX, along the Brazos River" +
                         " and connecting Highway 99 and 69, New Territory" +
                         " Trails (NTMTB) are 13 miles of unique single" +
                         " track.  Difficulties range from beginner to" +
                         " intermediate. Riders can explore flats, pumping" +
                         " segments, punchy climbs, drops as well as several" +
                         " unique obstacles.  The network is comprised of" +
                         " single track, double track & gravel that have" +
                         " both loops and out/back segments that can gross a" +
                         " total 21+ miles per full tour.",
      website:           'http://ghorba.org/trails/river-bend',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/rbt_map.jpg',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

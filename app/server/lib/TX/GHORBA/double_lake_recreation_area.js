Meteor.startup(function() {
  var name = 'Double Lake Recreation Area';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Double Lake',
      type:              'Singletrack',
      ascent:            380,
      descent:           -380,
      miles:             21,
      level:             'Beginner/Intermediate',
      city:              'Coldspring, TX',
      address:           'Coldspring Intermediate School, 1510 Texas 150, Coldspring, TX 77331',
      location:          {
        type:        'Point',
        coordinates: [-95.115541, 30.573697 ]
      },
      description:       "Located a short drive north of Houston, Double" +
                         " Lake is a trail that invokes positive response" +
                         " and memories from the riders that have ridden" +
                         " there. The trail system loops through the forest" +
                         " and is perfect for beginners, young riders as" +
                         " well as experienced riders who want to go fast." +
                         " Double Lake is home to the annual Big Ring" +
                         " Challenge (BRC) race and was expanded in 2011 by" +
                         " GHORBA made possible by grants from Texas Parks" +
                         " and Wildlife, Bikes Belong, and member" +
                         " contributions. Unlike many trails in the Houston" +
                         " area, the full eighteen (18) miles of this trail" +
                         " are dirt single track that is built to be ridden" +
                         " fast with fast and swoopy turns, rolling single" +
                         " track, and a clean compacted trail surface." +
                         " Boredom at Double Lake means you need to be" +
                         " riding faster. Though often touted as a trail" +
                         " that dries very fast after a rain, please be" +
                         " mindful that the trail can still be damaged when" +
                         " ridden while wet. Still, this is the place to go" +
                         " when trails closer to Houston are too wet to" +
                         " ride, as it drains well and the coarse sand" +
                         " actually is faster after it gets a bit damp." +
                         " Please note that there is a recreation area use" +
                         " fee per vehicle, payable on entry. Contact the" +
                         " Trail Steward" +
                         " http://ghorba.org/contact/double-lake-recreational-area",
      website:           'http://ghorba.org/trails/double-lake-recreation-area',
      map:               'http://cdn.ghorba.org/sites/ghorba.org/files/www/trails/map/double-lake-recreation-area.png',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

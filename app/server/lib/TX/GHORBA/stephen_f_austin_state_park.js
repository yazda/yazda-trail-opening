Meteor.startup(function() {
  var name = 'Stephen F. Austin State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Stephen F. Austin',
      type:              'Singletrack',
      ascent:            20,
      descent:           -20,
      miles:             6.3,
      level:             'Beginner',
      city:              'Sealy, TX',
      address:           '1130 Park Road 38, Sealy, TX 77474',
      location:          {
        type:        'Point',
        coordinates: [-96.109753, 29.815063]
      },
      description:       "Stephen F Austin State Park (SFA) is a hidden gem" +
                         " west of Houston. Though this trail does not" +
                         " feature any technical features to speak of, the" +
                         " layout and flow of the trail make for a fast ride" +
                         " similar to Double Lake. Swooping through the" +
                         " trees along the banks of the Brazos River the" +
                         " trails at SFA are smooth and flowy unlike the" +
                         " root gardens of Houston proper. Combined with" +
                         " gradual elevation changes and a few steep hills" +
                         " makes for a fun, fast ride . SFA’s trails are" +
                         " multi-use and two-way so always use caution on" +
                         " these and always be in control of your speed." +
                         " Contact the Trail Steward" +
                         " http://ghorba.org/contact/stephen-f-austin-state-park",
      website:           'http://ghorba.org/trails/stephen-f-austin-state-park',
      status:            'caution',
      twitter_tags:      '#GHORBATrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

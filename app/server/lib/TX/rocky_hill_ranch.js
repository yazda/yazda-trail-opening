Meteor.startup(function() {
  var name = 'Rocky Hill Ranch';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Rocky Hill Ranch',
      type:              'Singletrack',
      ascent:            2000,
      descent:           -2000,
      miles:             22,
      level:             'Intermediate/Advanced',
      city:              'Smithville, TX',
      address:           '578 FM 153, Smithville, TX 78957',
      location:          {
        type:        'Point',
        coordinates: [-97.1276512, 30.0439611]
      },
      description:       "Rocky Hill is the best 45 minute trip that you can" +
                         " make from Austin. The main loop is about 26 miles" +
                         " total and you'll encounter a wide variety of" +
                         " trails, from the straightforward jeep trails to" +
                         " ledges, short climbs, twists through the trees," +
                         " bunny hops over logs and more. And Fat Chuck is" +
                         " one hell of a climb! Although there are many" +
                         " areas that the trial is actually rocky, it might" +
                         " as we have been named 'Sandy Hill Ranch' because" +
                         " there are more sandy trails than anything else." +
                         " Every time I leave, I end up cleaning out about a" +
                         " pound of sand out of my drive train. Once you" +
                         " finish your ride, on some days the cafe is open," +
                         " but not what it used to be.",
      website:           'http://austinbike.com/index.php/east-of-austin/34-rhr',
      map:               'http://www.rockyhillranch.com/wp-content/uploads/2012/07/RHR-Map0110Web.pdf',
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

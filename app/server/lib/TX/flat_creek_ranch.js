Meteor.startup(function() {
  var name = 'Flat Creek Crossing Ranch';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Flat Creek',
      type:              'Singletrack',
      ascent:            500,
      descent:           -500,
      miles:             8,
      level:             'Intermediate/Advanced',
      city:              'Johnson City, TX',
      address:           'Ulrich Rd, Johnson City, TX 78636, USA',
      location:          {
        type:        'Point',
        coordinates: [-98.2247739, 30.2885932]
      },
      description:       "The Ranch contains both an 8 mile singletrack loop as well as some recently added downhill sections.",
      website:           "https://www.facebook.com/FlatCreekCrossingRanch/",
      map:               'https://www.google.com/maps/place/Flat+Creek+Crossing+Ranch/@30.2865019,-98.226693,15z/data=!4m5!3m4!1s0x0:0xac32f7d8acdd052c!8m2!3d30.2879481!4d-98.2248451',
      phone:             '(512) 689 0483',
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Monte Bella - RGV';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "Monte Bella",
      type:              'Singletrack',
      ascent:            5,
      descent:           -5,
      miles:             6,
      level:             'Beginner',
      city:              'Brownsville, TX',
      address:           'W Alton Gloor Blvd, Brownsville, TX 78520, USA',
      location:          {
        type:        'Point',
        coordinates: [-97.541087, 25.9533733]
      },
      description:       "Monte Bella Hike and Bike Trails are a 6 mile" +
                         " network of single track trails.  Minor 5 ft" +
                         " elevation changes keep things interesting but for" +
                         " the most part the trails are twisty and fast on a" +
                         " MTN bike and a great place to develop basic" +
                         " skills as well as high speed turning.",
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

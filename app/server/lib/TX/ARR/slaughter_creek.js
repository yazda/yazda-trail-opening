Meteor.startup(function() {
  var name = 'Slaughter Creek';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Slaughter Creek',
      type:              'Singletrack',
      ascent:            140,
      descent:           -140,
      miles:             5,
      level:             'Beginner',
      city:              'Austin, TX',
      address:           '9901 Farm to Market Rd 1826, Austin, TX 78737',
      location:          {
        type:        'Point',
        coordinates: [-97.904379, 30.207005]
      },
      description:       "Slaughter Creek is a beginner trail in Austin that" +
                         " is perfect for bringing out people new to the" +
                         " sport, spouses or children who are just getting" +
                         " started. The trails are really easy to ride, well" +
                         " designed and well maintained. The park is" +
                         " available for biking, hiking and horseback" +
                         " riding, so you do have to keep in mind that you" +
                         " are sharing the trails with others. The horses" +
                         " start in one direction and bikers start in the" +
                         " other, so, theoretically, you would not be coming" +
                         " up behind the horses, which can spook them. The" +
                         " park is located in southwest Austin, out on the" +
                         " way to Driftwood (think Salt Lick). When you" +
                         " drive out, be very careful when you get onto" +
                         " 1826, it is easy to miss your turn.",
      website:           'http://austinbike.com/index.php/austin/9-slaughter',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

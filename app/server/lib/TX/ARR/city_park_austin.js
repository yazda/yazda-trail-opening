Meteor.startup(function() {
  var name = 'City Park - Austin';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'City Park',
      type:              'Singletrack',
      ascent:            850,
      descent:           -850,
      miles:             6,
      level:             'Advanced',
      city:              'Austin, TX',
      address:           '7544 Oak Shores Drive, Austin, TX',
      location:          {
        type:        'Point',
        coordinates: [-97.8292339, 30.3429551]
      },
      description:       "Ouch. Practice that word. If you want technical," +
                         " this is your park. Think you got game? Think" +
                         " again. Emma is out for blood and she will get it" +
                         " from you, it's just a matter of time. Known and" +
                         " feared throughout the Austin biking community," +
                         " City Park seperates the riders from the posers." +
                         " This is not recommended for beginners unless they" +
                         " are riding with someone else that has the" +
                         " patience to teach. There is so much to learn at" +
                         " CP that if you have the right group, you can" +
                         " spend an entire morning working on 5 or 6 climbs." +
                         " But when you walk away your skills will be" +
                         " significantly better and your confidence will" +
                         " skyrocket. City Park is not only a bike trail," +
                         " but also a motorcycle trail. Share the trail is" +
                         " your responsibility because these guys ride fast" +
                         " and can't hear you. Just like a Yugo and a" +
                         " Hummer, assume that you have to yield and make" +
                         " yourself visible at all times. The trail is" +
                         " strewn with dozens of limestone ledges, from the" +
                         " straightforward and simple through the the most" +
                         " complex (the triple bitch) of ledges that will" +
                         " test your skills and patience. For beginners," +
                         " assume that you will walk up a lot of these. A" +
                         " good exercise is to cound the number of times you" +
                         " have to dismount. Every time you come back you'll" +
                         " be stronger and that number should get smaller." +
                         " It's a blast, but be careful, you can really rack" +
                         " yourself up if you're not paying attention.",
      website:           "http://austinbike.com/index.php/austin/6-citypark",
      map:               'https://www.austintexas.gov/sites/default/files/files/Parks/GIS/EmmaLongKioskMap_Motorcycle.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

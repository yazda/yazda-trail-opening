Meteor.startup(function() {
  var name = 'Reveille Peak Ranch';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Reveille',
      type:              'Singletrack',
      ascent:            1200,
      descent:           -1200,
      miles:             14.5,
      level:             'Intermediate/Advanced',
      city:              'Burnet, TX',
      address:           '105 CR 114, Burnet, TX',
      location:          {
        type:        'Point',
        coordinates: [-98.350473, 30.811208]
      },
      description:       "Reveille Peak Ranch is north and west of Austin," +
                         " out near Burnet, TX. The trail is granite which" +
                         " is significantly different from the limestone" +
                         " that you encounter in Austin. There are places" +
                         " where you cannot believe that you can actually" +
                         " grip the rock when you climb, but, amazingly," +
                         " your tire will 'stick' to the granite and" +
                         " climbing is easy. At least that part. There are" +
                         " plenty of different obstacles and plenty of" +
                         " variance in the trail. Everything from sandy jeep" +
                         " roads to highly technical trail features that" +
                         " only the bravest will tackle. We did not have any" +
                         " flats on our ride, but rumor has it that chances" +
                         " are much higher here; the combination of newer" +
                         " trail and cactus leave you with greater" +
                         " probability. The pavilion has a view of the small" +
                         " lake and there are showers along with a host of" +
                         " other amenities that one typically does not find" +
                         " on an MTB trail.",
      website:           'http://austinbike.com/index.php/north-of-austin/22-rpr',
      map:               'http://rprtexas.com/wordpress/wp-content/uploads/2011/02/RPR-trail-map-v4_2.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

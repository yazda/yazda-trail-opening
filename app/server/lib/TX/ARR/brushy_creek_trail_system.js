Meteor.startup(function() {
  var name = 'Brushy Creek Trail System';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Brushy Creek',
      type:              'Singletrack',
      ascent:            400,
      descent:           -400,
      miles:             10,
      level:             'Intermediate/Advanced',
      city:              'Cedar Park, TX',
      address:           '2310 Brushy Creek Rd, Cedar Park, TX 78613',
      location:          {
        type:        'Point',
        coordinates: [-97.779383, 30.5022627]
      },
      description:       "The Brushy Creek Regional Trail is a great example" +
                         " of the trail builders working with the city to" +
                         " get more singletrack for biking cut into the" +
                         " Austin area. The trail consists of multiple" +
                         " sections, including 'Mulligan', near the golf" +
                         " course, 'Picnic' which is along the creek and" +
                         " 'Deception', the largest of the trails. The" +
                         " Picnic and Mulligan are about 1.75M total between" +
                         " the two, with Deception being the largest of the" +
                         " 3 at about 6 miles. It is packed in, like a small" +
                         " intestine, with plenty of switchbacks and turns.",
      website:           'http://austinbike.com/index.php/north-of-austin/15-brushy',
      map:               'http://www.wilco.org/Portals/0/MapSide3.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

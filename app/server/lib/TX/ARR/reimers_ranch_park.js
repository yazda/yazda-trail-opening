Meteor.startup(function() {
  var name = 'Reimers Ranch Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Reimers Ranch',
      type:              'Singletrack',
      ascent:            800,
      descent:           -800,
      miles:             13,
      level:             'Beginner/Intermediate',
      city:              'Dripping Springs, TX',
      address:           '23610 Hamilton Pool Rd, Dripping Springs, TX 78620',
      location:          {
        type:        'Point',
        coordinates: [-98.129196,30.350954]
      },
      description:       "Reimer's Ranch was originally a rock climbing and" +
                         " hunting ranch out in the outskirts of Austin, but" +
                         " they added mountain biking to the list of" +
                         " activities. David Cardosa oversaw the trails and" +
                         " built most of it himself - and did an excellent" +
                         " job at that. The trails are fast - really fast." +
                         " With clean lines, broad switchbacks and open" +
                         " spaces, you can really haul across the trails." +
                         " The signage on the trails is similar to the signs" +
                         " that you see on ski slopes to denote the skill" +
                         " level, green, blue and black. The ground is very" +
                         " open, so be sure to not only bring water with you" +
                         " but also keep some in a cooler back at your car." +
                         " Dehydration is very easy out here with all of the" +
                         " direct sunlight. Since the county has taken over" +
                         " the facilities have really been upgraded.",
      website:           'http://austinbike.com/index.php/west-of-austin/42-reimers',
      map:               'http://laketravislifestyle.com/wp-content/uploads/2013/09/ReimersRanch-Mountain-Bike-Map.png',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

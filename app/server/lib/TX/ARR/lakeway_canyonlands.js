Meteor.startup(function() {
  var name = 'Lakeway / Canyonlands';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Lakeway / Canyonlands',
      type:              'Singletrack',
      ascent:            1222,
      descent:           -1222,
      miles:             10.1,
      level:             'Intermediate/Advanced',
      city:              'Lakeway, TX',
      address:           'Trophy Dr, Lakeway, TX 78738',
      location:          {
        type:        'Point',
        coordinates: [-97.9987787, 30.349877]
      },
      description:       "Lakeway has built out several trails that are" +
                         " accessible to the public featuring a variety of" +
                         " trail surfaces. They range from doubletrack and" +
                         " crushed stone jogging trails to singletrack with" +
                         " some technical climbs. Two of the three trails" +
                         " are located next to each other with the third" +
                         " being a little over a mile away, so you have to" +
                         " add some street/jogging trail mileage to your ride.",
      website:           'http://austinbike.com/index.php/west-of-austin/38-lakeway',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

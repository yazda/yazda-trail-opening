Meteor.startup(function() {
  var name = 'Pedernales Falls State Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Pedernales',
      type:              'Singletrack',
      ascent:            500,
      descent:           -500,
      miles:             10,
      level:             'Intermediate',
      city:              'Johnson City, TX',
      address:           '2585 Park Rd 6026, Johnson City, TX 78636',
      location:          {
        type:        'Point',
        coordinates: [-98.248329, 30.32473]
      },
      description:       "The six-mile Wolf Mountain Trail wraps around" +
                         " Tobacco and Wolf mountains and winds along the" +
                         " small canyons created by Mescal and Tobacco" +
                         " creeks. The newer Juniper Ridge technical, ten" +
                         " mile single track trail provides plenty of" +
                         " obstacles for more advanced riders.",
      website:           'http://austinbike.com/index.php/west-of-austin/41-pedernalesfalls',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

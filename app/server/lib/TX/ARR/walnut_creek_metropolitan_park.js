Meteor.startup(function() {
  var name = 'Walnut Creek Metropolitan Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Walnut Creek',
      type:              'Singletrack',
      ascent:            700,
      descent:           -700,
      miles:             12,
      level:             'Beginner/Intermediate',
      city:              'Austin, TX',
      address:           '12138 N Lamar Blvd, Austin, TX 78753',
      location:          {
        type:        'Point',
        coordinates: [-97.68391,30.40073 ]
      },
      description:       "Walnut Creek is the best spot in Austin for" +
                         " beginners. It's not very technical and is heavily" +
                         " travelled. That means you'll have a better chance" +
                         " of being able to pick up other riders and have" +
                         " people show you around. WC is a tangle of trails" +
                         " that go all over the park - they are two way" +
                         " trails, so keep an eye on where you are going." +
                         " There is a large 11-12 mile 'loop' that many" +
                         " people ride, this starts from the parking lot by" +
                         " rhe pool, you start out to your right, past the" +
                         " maintenance shed and follow the arrow trail" +
                         " markers. There are 3-4 creek crossings that can" +
                         " be anywhere from bone dry to a foot deep" +
                         " depending on the time of year. Watch out for" +
                         " roots and loose dirt. Because it is heavily" +
                         " travelled, WC can be subject to heavier erosion" +
                         " so stay on the trails, don't widen ruts and do" +
                         " NOT ride it after it has rained. Directions: From" +
                         " I-35 exit Parmer and head west. The first" +
                         " stoplight is Lamar, go south and turn in on the" +
                         " right just past the Vietnamese temple at Yager." +
                         " Park is clearly marked. From MoPac exit Parmer" +
                         " and head east. About 1/4 mile before Lamar, take" +
                         " a right on Willow Wild and follow that into the" +
                         " park. ",
      website:           'http://austinbike.com/index.php/austin/13-walnut',
      map:               'http://www.mountainbiketx.com/downloads/texas/maps/Walnut_Creek_II.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

Meteor.startup(function() {
  var name = 'Barton Creek Green Belt';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Barton Creek',
      type:              'Singletrack',
      ascent:            125,
      descent:           -125,
      miles:             15,
      level:             'Intermediate/Advanced',
      city:              'Austin, TX',
      address:           '3755 S Capital of Texas Hwy, Austin, TX 78704',
      location:          {
        type:        'Point',
        coordinates: [-97.8022364, 30.2431636]
      },
      description:       "The Barton Creek Greenbelt is referred to as the" +
                         " 'crown jewel of Austin' for a good reason - this" +
                         " trail represents all that is great about Austin." +
                         " It's wild, accessible and right in the heart of" +
                         " the city. What other city has a trail like this" +
                         " where you can be right in the middle of" +
                         " everything and in only a few minutes be" +
                         " completely lost in nature? The greenbelt is a" +
                         " fixture for Austin mountain bikers and offers a" +
                         " variety of trail conditions, obstacles and" +
                         " challenges. The trail is based on a main trail" +
                         " that is about seven miles in length. In addition" +
                         " to the main trail there are several 'back trails'" +
                         " that have been built over the years. During the" +
                         " rainy season the trail can get very muddy and" +
                         " will close down. Do NOT attempt to ride if the" +
                         " trail is closed - that's a good way to get bikers" +
                         " banned from the trail. There are already enough" +
                         " 'share the trail' conflicts with bikers and" +
                         " joggers/dogs that we don't need anyone else" +
                         " ruining it for everyone.",
      phone:             '(512) 974 1250',
      website:           'http://austinbike.com/index.php/austin/2-bcgb',
      map:               'http://www.mountainbiketx.com/downloads/texas/maps/Barton_Creek_Greenbelt_II.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

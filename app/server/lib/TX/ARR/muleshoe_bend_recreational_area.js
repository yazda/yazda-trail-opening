Meteor.startup(function() {
  var name = 'Muleshoe Bend Recreational Area';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Muleshoe Bend',
      type:              'Singletrack',
      ascent:            800,
      descent:           -800,
      miles:             8,
      level:             'Intermediate/Advanced',
      city:              'Spicewood, TX',
      address:           '2820 County Rd. 414 Spicewood, TX 78669',
      location:          {
        type:        'Point',
        coordinates: [-98.0921153, 30.4909023]
      },
      description:       "Muleshoe is a great place to do a few laps," +
                         " regardless of the weather. After a rain, Muleshoe" +
                         " usually recovers in a little over 24 hours. The" +
                         " park is essentially a large loop with a road that" +
                         " runs down the middle. Even if you get halfway" +
                         " around a loop and want to cut out, there is a" +
                         " place to bail and get back to the trailhead.",
      website:           'http://austinbike.com/index.php/west-of-austin/39-muleshoe',
      map:               'http://wherethetrailsare.com/wp-content/uploads/2012/05/muleshoe-park.jpg',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

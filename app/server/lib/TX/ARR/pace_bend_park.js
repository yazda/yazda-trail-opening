Meteor.startup(function() {
  var name = 'Pace Bend Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         'Pace Bend',
      type:              'Singletrack',
      ascent:            850,
      descent:           -850,
      miles:             11,
      level:             'Intermediate/Advanced',
      city:              'Spicewood, TX',
      address:           '2805 Pace Bend Rd. North Spicewood, TX 78734',
      location:          {
        type:        'Point',
        coordinates: [-98.098048, 30.440893]
      },
      description:       "Opened in 2008, this is a great example of local" +
                         " teams working with the park to engage and create" +
                         " a real mountain biking destination that includes" +
                         " races and events. Close in proximity to Muleshoe," +
                         " the conditions are fairly similar, but there is" +
                         " more elevation change, more interesting downhill" +
                         " sections and more trail variance.",
      website:           "http://austinbike.com/index.php/west-of-austin/40-pb",
      map:               'https://parks.traviscountytx.gov/images/docs/parks/pace_bend/pacebend_trailmap.pdf',
      status:            'caution',
      twitter_tags:      '#ARRTrails'
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

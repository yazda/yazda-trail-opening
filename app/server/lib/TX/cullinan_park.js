Meteor.startup(function() {
  var name = 'Cullinan Park';
  var trail = Trails.findOne({name: name});

  if(!trail) {
    trail = {
      createdAt:         new Date(),
      lastUpdated:       new Date(),
      availableStatuses: ['open', 'caution', 'closed', 'snow'],
      name:              name,
      shortName:         "Cullinan",
      type:              'Singletrack',
      ascent:            5,
      descent:           -5,
      miles:             5,
      level:             'Beginner/Intermediate',
      city:              'Sugar Land, TX',
      address:           'S Texas 6, Sugar Land, TX 77498, USA',
      location:          {
        type:        'Point',
        coordinates: [-95.66052890000003, 29.6351668]
      },
      description:       "Cullinan Park features several miles of single" +
                         " track and jeep roads.  There are several areas to" +
                         " the 756 acre park.  The main parkinglot gives" +
                         " acess to a 3 mile loop with several more miles" +
                         " across the Old Richmond Road bridge on the west" +
                         " side of the park.",
      status:            'caution',
      twitter_tags:      ''
    };

    Trails.insert(trail, function(error, id) {
    });
  }
});

//if(process.env.GA_TRACKING_ID) {
//  Meteor.settings.public.ga = {
//    id:          process.env.GA_TRACKING_ID,
//    trackUserId: true
//  }
//}

Meteor.startup(function() {
  if(process.env.METEOR_SETTINGS_PRODUCTION) {
    Meteor.settings = Npm.require(process.env.METEOR_SETTINGS_PRODUCTION);

    // If you have public settings that need to be exposed to the client,
    // you can set them like this
    if (Meteor.settings && Meteor.settings.public) {
      __meteor_runtime_config__.PUBLIC_SETTINGS = Meteor.settings.public;
    }
  }
});

Meteor.startup(function() {
  var superUsers = Meteor.users.find({
    $or: [
      {
        'emails.address': {
          $in: ['james.stoup@gmail.com']
        }
      },
      {
        "services.facebook.email": {$in: ["james@stoup.me", 'a.sieradzki@verizon.net']}
      }
    ]
  }, {fields: {_id: 1}}).fetch();

  Roles.addUsersToRoles(_.pluck(superUsers, '_id'), ['trail-images', 'add-trails'], Roles.GLOBAL_GROUP);

  Trails._ensureIndex({'location': '2dsphere'});
  Meteor.users._ensureIndex({'profile.userLocation': '2dsphere'});
  try {
    Meteor.users._dropIndex('profile.location_2dsphere');
  } catch(e) {
    console.log('profile.location_2dsphere does not exist');
  }

  Trails._ensureIndex({
        city:        'text',
        level:       'text',
        description: 'text',
        name:        'text'
      },
      {
        weights: {
          name:        10,
          description: 5,
          city:        3
        }
      });

  Push.debug = true;
  Push.addListener('token', function(token) {
    console.log('token received: ' + JSON.stringify(token));
  });

  function stravaUpdates() {
    var stravaUsers = Meteor.users.find({'services.strava': {$exists: true}}).fetch();
    var after = moment().subtract(4, 'hours').unix();

    _.each(stravaUsers, function(user) {
      Meteor._sleepForMs(1500);

      try {
        var response = Meteor.http.call('GET',
            'https://www.strava.com/api/v3/athlete/activities',
            {
              query:   'after=' + after,
              headers: {Authorization: 'Bearer ' + user.services.strava.accessToken}
            });

        var activity = _.last(response.data);

        if(activity && user.profile.lastStravaActivity !== activity.id) {
          var lat = activity.start_latitude;
          var lon = activity.start_longitude;

          var trail = Trails.findOne({
            'location': {
              $near: {
                $geometry:    {
                  type:        'Point',
                  coordinates: [lon, lat]
                },
                $maxDistance: (160934 / 100) * 5
              }
            }
          });

          console.log(trail);
          if(trail && !trail.requiresPermissions) {
            var text = 'Did you ride at ' + trail.shortName + '? Update the' +
                ' conditions so others in your area have the latest conditions';

            Push.send({
              from:    'Trail Status',
              title:   'Trail Status',
              text:    text,
              badge:   1, // TODO better badges
              payload: {
                title: 'Trail Status'
              },
              query:   {userId: user._id}
            });
          }

          Meteor.users.update(user._id, {$set: {'profile.lastStravaActivity': activity.id}});
        }
      }catch(e){
        console.log(e);
      }
    });
  }

  SyncedCron.add({
    name:     'Strava Check',
    schedule: function(parser) {
      return parser.text('every 1 hour');
    },
    job:      stravaUpdates
  });

  SyncedCron.start();
});

sitemaps.add('/sitemap.xml', function() {
  var sitemap = [];
  var trails = Trails.find().fetch();

  _.each(trails, function(trail) {
    var images = _.map(trail.images, function(image) {
      if(image.url) {
        return {loc: ('https://cdn.trail-status.com/' + image.url)}
      } else {
        return {loc: ('https://cdn.trail-status.com/' + image)}
      }
    });

    var obj = {
      page:       'trails/' + trail._id,
      lastmod:    trail.updatedAt,
      changefreq: 'daily',
      images:     images
    };

    var map = {
      page:       'trails/' + trail._id + '/map',
      lastmod:    trail.updatedAt,
      changefreq: 'daily'
    };

    sitemap.push(obj);
    sitemap.push(map);
  });

  sitemap.push({
    page:       '/',
    lastmod:    new Date(),
    changefreq: 'monthly'
  });

  sitemap.push({
    page:       'trails',
    lastmod:    new Date(),
    changefreq: 'weekly',

  });
  return sitemap;
});

/**
 * Meteor.publish('items', function (param1, param2) {
 *  this.ready();
 * });
 */

Meteor.publish('services', function() {
  return Meteor.users.find(this.userId, {
    fields: {
      'services.facebook.email': 1,
      'services.facebook.name': 1,
      'services.strava.email': 1
    }
  });
});

Meteor.publish('images', function() {
  return Images.find();
});

Meteor.publish('images-count', function() {
  Counts.publish(this, 'images-count', Images.find());
});

Meteor.publish('trails-count', function() {
  Counts.publish(this, 'trails-count', PendingTrails.find());
});

Meteor.publish('trails-search', function(string, limit) {
  if(limit > Trails.find().count()) {
    limit = 0;
  }

  if(!string || string.trim().length ===0) {
    return Trails.find({},{limit: limit});
  } else {
    return Trails.find(
        {$text: {$search: string}},
        {score: {$meta: 'textScore'}},
        {sort: {score: {$meta: 'textScore'}}, limit: limit});
  }
});

Meteor.publish('trails-query-count', function(query, search) {
  Counts.publish(this, 'trails-query-count', Trails.find(query))
});

Meteor.publish('pending-trails', function() {
  return PendingTrails.find({}, {sort: {name: -1}});
});

Meteor.publish('trail', function(_id, lat, lon) {
  if(_id) {
    return Trails.find({_id: _id});
  }
});

Meteor.publish('trails', function(query, limit) {
  if(limit > Trails.find().count()) {
    limit = Trails.find().count();
  }

  return Trails.find(query, { limit: limit });
});

Meteor.publish('usersForTrailStatus', function() {
  if(Roles.userIsInRole(this.userId, ['manage-users'])) {
    //TODO by group
    return Roles.getUsersInRole(['manage-users', 'manage-status']);
  } else {
    this.stop();
    return;
  }
});

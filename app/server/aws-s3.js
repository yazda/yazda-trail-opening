S3.config = {
  key:    process.env.AWS_S3_KEY,
  secret: process.env.AWS_S3_SECRET,
  bucket: process.env.AWS_S3_BUCKET
};

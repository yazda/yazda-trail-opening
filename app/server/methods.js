/*****************************************************************************/
/* Server Only Methods */
/*****************************************************************************/
Meteor.methods({
  /*
   * Example:
   *
   * '/app/items/insert': function (item) {
   * }
   */
  approveImage:       function(userId, _id) {
    if(Roles.userIsInRole(userId, 'trail-images')) {
      var image = Images.findOne({_id: _id});
      delete image.trail;

      Trails.update({_id: image.trailId},
          {$push: {images: image}},
          function(err) {
            if(!err) {
              Images.remove(_id);
            }
          });
    } else {
      throw new Meteor.Error('permissions', 'You do not have permission to' +
          ' approve images');
    }
  },
  rejectImage:        function(userId, _id) {
    if(Roles.userIsInRole(userId, 'trail-images')) {
      var image = Images.findOne({_id: _id});

      Images.remove(_id);
    } else {
      throw new Meteor.Error('permissions', 'You do not have permission to' +
          ' reject images');
    }
  },
  newImageUploaded:   function(_id) {
    var users = Roles.getUsersInRole('trail-images', _id).fetch();
    var userIds = _.pluck(users, '_id');

    Push.send({
      from:    'Trail Status',
      title:   'Trail Status',
      text:    "New image uploaded to Trail Status. Please approve",
      payload: {
        title: 'Trail Status'
      },
      query:   {userId: {$in: userIds}}
    });
  },
  newTrailCreated:    function(_id) {
    var users = Roles.getUsersInRole('add-trails', _id).fetch();
    var userIds = _.pluck(users, '_id');

    Push.send({
      from:    'Trail Status',
      title:   'Trail Status',
      text:    "New trail created in Trail Status.",
      payload: {
        title: 'Trail Status'
      },
      query:   {userId: {$in: userIds}}
    });
  },
  createPendingTrail: function(name, location, ascent, descent, description, userId) {
    if(!userId) {
      throw new Meteor.Error('permissions', 'You do not have permission to' +
          ' add new trails.');
    }

    if(!name || !location || !ascent || !descent) {
      throw new Meteor.Error('required-fields', 'Name, ascent, descent, and' +
          ' location are required');
    }

    PendingTrails.insert({
      name:        name,
      location:    location,
      ascent:      ascent,
      descent:     descent,
      description: description,
      userId:      userId
    });
  },
  addTrailImage:      function(image) {
    Images.insert(image);
  },
  updateTrailStatus:  function(_id, status, comment) {
    var trail = Trails.findOne({_id: _id});
    var hasPermission = (!trail.requiresPermissions || Roles.userIsInRole(Meteor.userId(), 'manage-status', _id));

    if(!hasPermission) {
      throw new Meteor.Error('permissions', 'You do not have permission to change status');
    }

    var change = StatusChanges.findOne({trailId: _id}, {sort: {updatedAt: -1}});

    if(change && !moment(change.updatedAt).add(5, 'm').isSameOrBefore()) {
      throw new Meteor.Error('rate-limit', 'Trail Status was just changed.' +
          ' Give it some time before changing.');
    }

    Trails.update({_id: _id},
        {$set: {status: status, comment: comment}},
        function(error, docs) {
          if(!error) {
            var ids = Meteor.users.find(
                {'profile.trails': {$in: [_id]}},
                {fields: {_id: '1'}})
                .fetch();
            var userIds = _.pluck(ids, '_id');

            var trail = Trails.findOne({_id: _id});
            var text = trail.shortName + " status changed. now " + (trail[trail.status.toLowerCase() + 'Text'] || trail.status) + '!';

            console.log(text);
            Push.send({
              from:    'Trail Status',
              title:   'Trail Status',
              text:    text,
              badge:   1, // TODO better badges
              payload: {
                title: 'Trail Status'
              },
              query:   {userId: {$in: userIds}}
            });

            t = new Twit({
              consumer_key:        process.env.TWITTER_CONSUMER_KEY,
              consumer_secret:     process.env.TWITTER_CONSUMER_SECRET,
              access_token:        process.env.TWITTER_ACCESS_TOKEN,
              access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
            });

            t.post('statuses/update',
                {
                  status: text + ' - https://bnc.lt/download-trail-status' +
                          ' #TrailStatus ' + trail.twitter_tags,
                  lat:    trail.location.coordinates[1],
                  long:   trail.location.coordinates[0],
                });
          }
        });
  }
});

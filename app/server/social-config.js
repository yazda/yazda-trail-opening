// FACEBOOK
ServiceConfiguration.configurations.remove({
  service: 'facebook'
});

ServiceConfiguration.configurations.insert({
  service: 'facebook',
  appId:   Meteor.settings.FACEBOOK_APP_ID ? Meteor.settings.FACEBOOK_APP_ID : process.env.FACEBOOK_APP_ID,
  secret:  Meteor.settings.FACEBOOK_APP_SECRET ? Meteor.settings.FACEBOOK_APP_SECRET : process.env.FACEBOOK_APP_SECRET
});

// STRAVA
ServiceConfiguration.configurations.remove({
  service: "strava"
});

ServiceConfiguration.configurations.insert({
  service:   "strava",
  client_id: process.env.STRAVA_CLIENT_ID,
  secret:    process.env.STRAVA_CLIENT_SECRET
});

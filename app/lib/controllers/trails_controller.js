TrailsController = RouteController.extend({
  subscriptions: function() {
    // set up the subscriptions for the route and optionally
    // wait on them like this:
    //
    // this.subscribe('item', this.params._id).wait();
    //
    // "Waiting" on a subscription does not block. Instead,
    // the subscription handle is added to a reactive list
    // and when all items in this list are ready, this.ready()
    // returns true in any of your route functions.
    var lat = Session.get('latitude');
    var lon = Session.get('longitude');
    var query = {};
    var user = Meteor.user();
    var search = Session.get('searchQuery');

    if(lat && lon) {
      query = {
        'location': {
          $near: {
            $geometry: {
              type:        'Point',
              coordinates: [lon, lat]
            }
          }
        }
      };
    }

    if(Session.get('searchTrails')) {
      if(search && search.trim().length !== 0) {
        query = {$text: {$search: search}};
      }
    } else {
      if(user && user.profile && user.profile.trails) {
        query._id = {$in: user.profile.trails};
      } else {
        query._id = {$exists: false};
      }
    }

    this.subscribe('trails-query-count', query).wait();
    this.subscribe('trails', query, Session.get('queryLimit')).wait();
    this.subscribe('trail', this.params._id).wait();
  },

  data: function() {
    // return a global data context like this:
    // Items.findOne({_id: this.params._id});
    if(this.params._id) {
      return Trails.findOne({_id: this.params._id});
    } else {
      return Trails.find();
    }
  },

  index: function() {
    // You can create as many action functions as you'd like.
    // This is the primary function for running your route.
    // Usually it just renders a template to a page. But it
    // might also perform some conditional logic. Override
    // the data context by providing it as an option in the
    // last parameter.
    this.render('TrailsIndex', {/* data: {} */});
  },

  show: function() {
    this.render('TrailsShow', {});
  },

  new: function() {
    this.render('TrailsNew', {});
  },

  map: function() {
    this.render('TrailsShowMap');
  }
});

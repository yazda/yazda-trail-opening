HomeController = RouteController.extend({
  layoutTemplate: 'MasterLayout',

  subscriptions: function() {
    this.subscribe('trails-search', Session.get('searchQuery'), 5).wait();
  },

  action: function() {
    this.render('Home');
  }
});

AccountsTemplates.configure({
  defaultLayout:               'MasterLayout',
  // Behavior
  confirmPassword:             true,
  enablePasswordChange:        true,
  forbidClientAccountCreation: false,
  overrideLoginErrors:         true,
  sendVerificationEmail:       true,
  lowercaseUsername:           true,
  focusFirstInput:             true,

  // Appearance
  showAddRemoveServices:           false,
  showForgotPasswordLink:          true,
  showLabels:                      true,
  showPlaceholders:                true,
  showResendVerificationEmailLink: true,

  // Client-side Validation
  continuousValidation: false,
  negativeFeedback:     false,
  negativeValidation:   true,
  positiveValidation:   true,
  positiveFeedback:     true,
  showValidating:       true,

  // Privacy Policy and Terms of Use
  privacyUrl: 'https://yazdaapp.com/privacy',
  termsUrl: 'https://yazdaapp.com/terms-of-service',

  // Redirects
  homeRoutePath:   '/',
  redirectTimeout: 4000,

  // Hooks
  //onLogoutHook: myLogoutFunc,
  //onSubmitHook: mySubmitFunc,
  //preSignUpHook: myPreSubmitFunc,
  //postSignUpHook: postSignUpHook,

  // Texts
  //texts: {
  //  navSignIn: "signIn",
  //  navSignOut: "signOut",
  //  optionalField: "optional",
  //  pwdLink_pre: "",
  //  pwdLink_link: "forgotPassword",
  //  pwdLink_suff: "",
  //  resendVerificationEmailLink_pre: "Verification email lost?",
  //  resendVerificationEmailLink_link: "Send again",
  //  resendVerificationEmailLink_suff: "",
  //  sep: "OR",
  //  signInLink_pre: "ifYouAlreadyHaveAnAccount",
  //  signInLink_link: "signin",
  //  signInLink_suff: "",
  //  signUpLink_pre: "dontHaveAnAccount",
  //  signUpLink_link: "signUp",
  //  signUpLink_suff: "",
  //  socialAdd: "add",
  //  socialConfigure: "configure",
  //  socialIcons: {
  //    "meteor-developer": "fa fa-rocket",
  //  },
  //  socialRemove: "remove",
  //  socialSignIn: "signIn",
  //  socialSignUp: "signUp",
  //  socialWith: "with",
  //  termsPreamble: "clickAgree",
  //  termsPrivacy: "privacyPolicy",
  //  termsAnd: "and",
  //  termsTerms: "terms",
  //  title: {
  //    changePwd: "Password Title",
  //    enrollAccount: "Enroll Title",
  //    forgotPwd: "Forgot Pwd Title",
  //    resetPwd: "Reset Pwd Title",
  //    signIn: "Sign In Title",
  //    signUp: "Sign Up Title",
  //    verifyEmail: "Verify Email Title",
  //  }
  //}
});

if(Meteor.isClient) {
  Accounts.ui.config({
    requestPermissions: {
      facebook: ['public_profile', 'email']
    }
  });
}

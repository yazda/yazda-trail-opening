var fields = {
  _id:         true,
  name:        true,
  address:     true,
  lastUpdated: true,
  phone:       true,
  website:     true,
  status:      true,
  city:        true,
  level:       true,
  miles:       true,
  ascent:      true,
  descent:     true,
  type:        true,
  description: true,
  location:    true,
  comment:     true,
  images:      true
};

Router.route('/api/trails/:_id',
    function() {
      this.response.statusCode = 200;
      this.response.setHeader("Content-Type", "application/json");
      this.response.setHeader("Access-Control-Allow-Origin", "*");
      this.response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      this.response.end(JSON.stringify(
          Trails.findOne({_id: this.params._id})
      ));
    },
    {
      where: 'server',
      name:  'api.trails.show'
    }
);

Router.route('/api/trails',
    function() {
      var limit = 30;
      var query = {};

      if(this.params.q) {
        query.$text = '';
        limit = this.params.pageSize || 5;
      }

      if(this.params.query.area) {
        var coords = this.params.query.area.split(/\)\(/);

        var coordinates = _.map(coords, function(obj) {
          var cord = obj.replace(/[\(\)]/g, '').split(',');

          return [Number(cord[1]), Number(cord[0])];
        });

        query.location = {
          $geoWithin: {
            $geometry: {
              type:        'Polygon',
              coordinates: [coordinates]
            }
          }
        };
      } else if(this.params.query.lat) {
        var lat = Number(this.params.query.lat);
        var lon = Number(this.params.query.lon);
        var distance = Number(this.params.query.distance) || 10;

        query.location = {
          $near: {
            $geometry:    {
              type:        'Point',
              coordinates: [lon, lat]
            },
            $maxDistance: (160934 / 100) * distance
          }
        }
      }

      if(limit > 30) {
        limit = 30;
      }

      this.response.statusCode = 200;
      this.response.setHeader("Content-Type", "application/json");
      this.response.setHeader("Access-Control-Allow-Origin", "*");
      this.response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
      this.response.end(JSON.stringify(
          Trails.find(query, {limit: limit, fields: fields}).fetch()
      ));
    },
    {
      where: 'server',
      name:  'api.trails'
    }
);

Router.configure({
  layoutTemplate:   'MasterLayout',
  loadingTemplate:  'Loading',
  notFoundTemplate: 'NotFound',
  trackPageView:    true
});

//Accounts Templates Routes
AccountsTemplates.configureRoute('signIn');
AccountsTemplates.configureRoute('enrollAccount');
AccountsTemplates.configureRoute('changePwd');
AccountsTemplates.configureRoute('forgotPwd');
AccountsTemplates.configureRoute('resetPwd');
AccountsTemplates.configureRoute('signUp');
AccountsTemplates.configureRoute('verifyEmail');
AccountsTemplates.configureRoute('resendVerificationEmail');

Router.route('sign-out', {
  name:           'atSignOut',
  onBeforeAction: function() {
    var self = this;

    Meteor.logout(function(err) {
      if(!err) {
        Branch.logout();
      }
    });

    Router.go('home');
    this.next();
  }
});

Router.route('/', {
  name:           'home',
  layoutTemplate: 'HomeLayout',
  controller:     'HomeController',
  action:         'action',
  where:          'client',
  fastRender:     true,
  onBeforeAction: function() {
    //Redirect to trail page
    if(Meteor.isCordova) {
      Router.go('trails');
    }

    this.next();
  },
  // SEO
  seo:            {
    description: "Trail Status is the only app that alerts you the moment" +
                 " your favorite MTB trails are open or closed. Hit some" +
                 " gnarly trails as soon as they open!.",
    meta:        {
      keywords: ['trail status', 'trail conditions', 'mountain bike trail' +
      ' conditions']
    },
    image:       'https://cdn.trail-status.com/images/trailstatus-fb.png',
    twitter:     {
      card:                  'app',
      site:                  '@trail_status',
      'app:url:iphone':      'yazda-trail-status://',
      'app:url:googleplay':  'yazda-trail-status://',
      'app:id:iphone':       '1046598022',
      'app:id:googleplay':   'com.yazda.trail_status_android',
      'app:name:googleplay': 'Trail Status',
      'app:name:iphone':     'Trail Status'
    }
  }
});

Router.route('trails', {
  name:       'trails',
  controller: 'TrailsController',
  action:     'index',
  where:      'client',
  // SEO
  seo:        {
    title:       {
      text:      'Trail Status',
      separator: '|',
      suffix:    'Trails'
    },
    description: 'Check the status of mountain biking trails in your area. ',
    og:          {
      type: 'website'
    }
  }
});

Router.route('trails/new', {
  name:       'trails.new',
  controller: 'PendingTrailsController',
  action:     'new',
  where:      'client'
});

Router.route('trails/pending', {
  name:       'trails.pending',
  controller: 'PendingTrailsController',
  action:     'pending',
  where:      'client'
});

Router.route('trails/:_id', {
  name:          'trails.show',
  controller:    'TrailsController',
  action:        'show',
  where:         'client',
  fastRender:    true,
  onAfterAction: function() {
    Meta.set({
      name:     'property',
      property: 'place:location:longitude',
      content:  this.data().location.coordinates[0]
    });

    Meta.set({
      name:     'property',
      property: 'place:location:latitude',
      content:  this.data().location.coordinates[1]
    });
  },
  onStop:        function() {
    Meta.unset('place:location:longitude');
    Meta.unset('place:location:latitude');
    Meta.unset('og:image');
  },
  // SEO
  seo:           {
    title:       {
      text:      'Trail Status',
      separator: '|',
      suffix:    function() {
        return this.data().name;
      }
    },
    image:       function() {
      var images = this.data().images;
      var image;

      if(images) {
        if(images[0].url) {
          image = 'https://cdn.trail-status.com' + images[0].url;
        } else {
          image = 'https://cdn.trail-status.com/' + images[0];
        }
      }

      return image;
    },
    description: function() {
      return this.data().description;
    },
    twitter:     {
      card:  'summary_large_image',
      site:  '@trail_status',
      title: function() {
        return this.data().name;
      }
    },
    og:          {
      type:  'place',
      title: function() {
        return this.data().name;
      }
    }
  }
});

Router.route('trails/:_id/map', {
  name:          'trails.show.map',
  controller:    'TrailsController',
  action:        'map',
  where:         'client',
  onAfterAction: function() {
    Meta.set({
      name:     'property',
      property: 'place:location:longitude',
      content:  this.data().location.coordinates[0]
    });

    Meta.set({
      name:     'property',
      property: 'place:location:latitude',
      content:  this.data().location.coordinates[1]
    });
  },
  onStop:        function() {
    Meta.unset('place:location:longitude');
    Meta.unset('place:location:latitude');
    Meta.unset('og:image');
  },
  // SEO
  seo:           {
    title:       {
      text:      'Trail Status',
      separator: '|',
      suffix:    function() {
        return this.data().name;
      }
    },
    image:       function() {
      var images = this.data().images;
      var image;

      if(images) {
        if(images[0].url) {
          image = 'https://cdn.trail-status.com' + images[0].url;
        } else {
          image = 'https://cdn.trail-status.com/' + images[0];
        }
      }

      return image;
    },
    description: function() {
      return this.data().description;
    },
    twitter:     {
      card:  'summary_large_image',
      site:  '@trail_status',
      title: function() {
        return this.data().name;
      }
    },
    og:          {
      type:  'place',
      title: function() {
        return this.data().name;
      }
    }
  }
});

Router.route('images', {
  name:       'images',
  controller: 'ImagesController',
  action:     'index',
  where:      'client'
});

Router.route('wizard', {
  name:       'wizard',
  controller: 'WizardController',
  action:     'action',
  where:      'client'
});

//Router.route('users', {
//  name:       'users',
//  controller: 'UsersController',
//  action:     'index',
//  where:      'client'
//});

Router.onBeforeAction(function() {
      var user = Meteor.user();

      if(user && (!user.profile || !user.profile.wizard)) {
        Router.go('wizard');
      }

      this.next();
    },
    {
      except: [
        'api.trails.show',
        'api.trails',
        'atSignIn',
        'atEnrollAccount',
        'atChangePwd',
        'atForgotPwd',
        'atResetPwd',
        'atSignUp',
        'atVerifyEmail',
        'atResendVerificationEmail',
        'home'
      ]
    });

Router.onBeforeAction(function() {
      if(!Roles.userIsInRole(Meteor.userId(), 'trail-images')) {
        Router.go('trails');
      } else {
        this.next();
      }
    },
    {
      only: ['images']
    });

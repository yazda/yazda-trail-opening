if(Meteor.isClient) {
  Router.onBeforeAction(function() {
        if(!Meteor.user()) {
          Router.go('/');
        }

        this.next()
      },
      {
        only: ['users']
      });
}

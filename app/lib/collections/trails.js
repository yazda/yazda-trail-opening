Trails = new Mongo.Collection('trails');

Trails.before.update(function(userId, doc, fieldNames, modifier, options) {
  modifier.$set = modifier.$set || {};
  modifier.$set.lastUpdated = new Date();
});

Trails.after.update(function(userId, doc, fieldNames, modifier, options) {
  if(fieldNames.indexOf('status') !== -1) {
    StatusChanges.insert({
      trailId:   doc._id,
      updatedAt: doc.lastUpdated,
      oldStatus: this.previous.status,
      newStatus: doc.status,
      comment:   doc.comment,
      updatedBy: userId || 'system'
    });
  }
});

Trails.after.insert(function(userId, doc) {
  var lat = doc.location.coordinates[1];
  var lon = doc.location.coordinates[0];
  var text = doc.name + ' has been added. Check it out and receive' +
      ' notifications whenever the status changes.';

  var ids = Meteor.users.find({
        'profile.userLocation': {
          $near: {
            $geometry:    {
              type:        'Point',
              coordinates: [lon, lat]
            },
            $maxDistance: 160934 // 100 miles
          }
        }
      },
      {fields: {_id: '1'}}
  ).fetch();

  var userIds = _.pluck(ids, '_id');
  userIds.push(null);

  Push.send({
    from:    'Trail Status',
    title:   'Trail Status',
    text:    text,
    badge:   1, // TODO better badges
    payload: {
      title: 'Trail Status'
    },
    query:   {userId: {$in: userIds}}
  });
});

if(Meteor.isClient || Meteor.isCordova) {
  Trails.allow({
    insert: function(userId, doc) {
      return false;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
} else if(Meteor.isServer) {
  Trails.allow({
    insert: function(userId, doc) {
      return true;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
}

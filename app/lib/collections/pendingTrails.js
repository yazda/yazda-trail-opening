PendingTrails = new Mongo.Collection('pendingTrails');
AddressSchema =new SimpleSchema({
  fullAddress: {
    type: String
  },
  lat: {
    type: Number,
    decimal: true
  },
  lng: {
    type: Number,
    decimal: true
  },
  geometry: {
    type: Object,
    blackbox: true
  },
  placeId: {
    type: String
  },
  street: {
    type: String,
    max: 100
  },
  city: {
    type: String,
    max: 50
  },
  state: {
    type: String,
    regEx: /^A[LKSZRAEP]|C[AOT]|D[EC]|F[LM]|G[AU]|HI|I[ADLN]|K[SY]|LA|M[ADEHINOPST]|N[CDEHJMVY]|O[HKR]|P[ARW]|RI|S[CD]|T[NX]|UT|V[AIT]|W[AIVY]$/
  },
  zip: {
    type: String,
    regEx: /^[0-9]{5}$/
  },
  country: {
    type: String
  }
});

PendingTrails.attachSchema(new SimpleSchema({
  name:      {
    type:  String,
    label: "Name"
  },
  address: {
    type: AddressSchema,
    label: 'Address'
  },
  shortName: {
    type:  String,
    label: "Nickname"
  },
  description: {
    type: String,
    label: 'Description'
  },
  type:      {
    type:          String,
    label:         "Type",
    allowedValues: ['Singletrack', 'Pump Track', 'Dirt Jump',
      'Downhill Trail/Park'],
    autoform:      {
      options: {
        Singletrack:           'Singletrack',
        'Pump Track':          'Pump Track',
        'Dirt Jump':           'Dirt Jump',
        'Downhill Trail/Park': 'Downhill Trail/Park'
      }
    }
  },
  ascent:    {
    type:  Number,
    label: "Ascent"
  },
  descent:   {
    type:  Number,
    label: "Descent"
  },
  miles:     {
    type:  Number,
    label: 'Distance (miles)'
  },
  level:     {
    type:          String,
    label:         'Level',
    allowedValues: ['Beginner', 'Beginner/Intermediate', 'Intermediate',
      'Intermediate/Advanced', 'Advanced', 'Advanced/Expert', 'Expert'],
    autoform:      {
      options: {
        Beginner:                'Beginner',
        'Beginner/Intermediate': 'Beginner/Intermediate',
        Intermediate:            'Intermediate',
        'Intermediate/Advanced': 'Intermediate/Advanced',
        Advanced:                'Advanced',
        'Advanced/Expert':       'Advanced/Expert',
        Expert:                  'Expert'
      }
    }
  },
  website: {
    type: String,
    optional: true,
    label: 'Website (optional)'
  },
  phone: {
    type: String,
    optional: true,
    label: 'Contact Phone Number (optional)'
  },
  map: {
    type: String,
    optional: true,
    label: 'Map URL (optional)'
  },
  createdBy: {
    type: String,
    autoValue:function(){ return this.userId }
  }
}));

PendingTrails.after.insert(function() {
  Meteor.call('newTrailCreated', this._id);
});

if(Meteor.isClient || Meteor.isCordova) {
  PendingTrails.allow({
    insert: function(userId, doc) {
      return false;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
} else if(Meteor.isServer) {
  PendingTrails.allow({
    insert: function(userId, doc) {
      return true;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
}

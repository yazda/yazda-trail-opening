StatusChanges = new Mongo.Collection('status_changes');

if(Meteor.isClient || Meteor.isCordova) {
  StatusChanges.allow({
    insert: function(userId, doc) {
      return false;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
} else if(Meteor.isServer) {
  StatusChanges.allow({
    insert: function(userId, doc) {
      return true;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
}

Images = new Mongo.Collection('images', {
  transform: function(doc) {
    doc.trail = Trails.findOne({ _id: doc.trailId });

    return doc;
  }
});

Images.after.insert(function(userId, doc) {
  Meteor.call('newImageUploaded', this._id);
});

if(Meteor.isClient || Meteor.isCordova) {
  Images.allow({
    insert: function(userId, doc) {
      return false;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return false;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
} else if(Meteor.isServer) {
  Images.allow({
    insert: function(userId, doc) {
      return true;
    },

    update: function(userId, doc, fieldNames, modifier) {
      return true;
    },

    remove: function(userId, doc) {
      return false;
    }
  });
}

# This .sh file will be sourced before starting your application.
# You can use it to put environment variables you want accessible
# to the server side of your app by using process.env.MY_VAR
#
# Example:
# export MONGO_URL="mongodb://localhost:27017/myapp-development"
# export ROOT_URL="http://localhost:3000"

export MAIL_URL="smtp://127.0.0.1:1025"
export AWS_S3_KEY="test"
export AWS_S3_SECRET="test"
export AWS_S3_BUCKET="test"
export FACEBOOK_APP_ID="881735991914121"
export FACEBOOK_APP_SECRET="4975f1b362d0697bb8ad6797b2cd1767"

#!/usr/bin/env bash

rm -Rf elasticbeanstalk
rm -Rf build.zip

cd app

demeteorizer -a os.linux.x86_64 -o ../elasticbeanstalk

cd ..

zip -r build.zip elasticbeanstalk .ebextensions/ .elasticbeanstalk/ config/production
rm -Rf elasticbeanstalk

eb deploy production

rm -Rf build.zip
